package in.innasoft.apsrtc.utilities;

/**
 * Created by purushotham on 2/12/16.
 */

public class AppUrls
{
    public static String BASE_USRL = "http://learningslot.in/apsrtc/app/apk/";
    public static String GETTING_DISTRICTS = "district ";
    public static String GETTING_MANDALS = "mandal";
    public static String GETTING_VILLAGES = "village";


    public static String GETTING_INSTITUTIONNAME = "institution";
    public static String GETTING_COURSENAME = "institution_course";
    public static String GETTING_INSTADDRESS = "institution_address";
    public static String GETTING_COURSEDURATION = "institution_course_years";


    public static String GETTING_CENTERS_IN_ROUTE = "center";
    public static String GETTING_PASS_TYPE = "pass_type";
    public static String GETTING_FROM_STOPS = "from_village";
    public static String GETTING_TO_STOPS = "to_village";
    public static String GETTING_ROUTE_VIA = "via";

    public static String TEMP_REGISTRATIOM = "http://learningslot.in/apsrtc/qrreader/insert.php";

}
