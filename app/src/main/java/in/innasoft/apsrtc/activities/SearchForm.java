package in.innasoft.apsrtc.activities;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import in.innasoft.apsrtc.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SearchForm extends AppCompatActivity implements View.OnClickListener
{
    /*ID's*/
    TextView search_form_headding_tv,search_form_info_tv,search_form_value_tv,search_form_dob_tv,bottom_com_tv;
    TextView search_form_dob_txt;
    Spinner search_form_spinner;
    EditText search_form_data_edt;
    Button search_form_bt;
    LinearLayout search_form_ll, search_form_hs_ll;
    String item = "null";




    /*Variables*/
    Typeface typeface, typeface2;
    private int mYear, mMonth, mDay;
    String stringDOB;
    int falgpreviousID = 0,flagName = 0,flagOnlineRegisterID = 0,flagMobileNumber = 0,flagAAdhar = 0,flagEmailId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_form);

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        search_form_ll = (LinearLayout) findViewById(R.id.search_form_ll);
        search_form_hs_ll = (LinearLayout) findViewById(R.id.search_form_hs_ll);

        /*search_form_til = (TextInputLayout) findViewById(R.id.search_form_til);
        search_form_til.setTypeface(typeface2);

        search_form_dob_til = (TextInputLayout) findViewById(R.id.search_form_dob_til);
        search_form_dob_til.setTypeface(typeface2);
        search_form_dob_til.setOnClickListener(this);*/

//        search_form_headding_tv = (TextView) findViewById(R.id.search_form_headding_tv);
//        search_form_headding_tv.setTypeface(typeface);

        search_form_info_tv = (TextView) findViewById(R.id.search_form_info_tv);
        search_form_info_tv.setTypeface(typeface2);

        search_form_value_tv = (TextView) findViewById(R.id.search_form_value_tv);
        search_form_value_tv.setTypeface(typeface2);

        search_form_dob_tv = (TextView) findViewById(R.id.search_form_dob_tv);
        search_form_dob_tv.setTypeface(typeface2);

        bottom_com_tv = (TextView) findViewById(R.id.bottom_com_tv);
        bottom_com_tv.setTypeface(typeface);

        search_form_dob_txt = (TextView) findViewById(R.id.search_form_dob_txt);
        search_form_dob_txt.setTypeface(typeface2);
        search_form_dob_txt.setOnClickListener(this);

        search_form_spinner = (Spinner) findViewById(R.id.search_form_spinner);

        search_form_data_edt = (EditText) findViewById(R.id.search_form_data_edt);
        search_form_data_edt.setTypeface(typeface2);

        /*search_form_dob_edt = (EditText) findViewById(R.id.search_form_dob_edt);
        search_form_dob_edt.setTypeface(typeface2);
        search_form_dob_edt.setOnClickListener(this);*/

        search_form_bt = (Button) findViewById(R.id.search_form_bt);
        search_form_bt.setTypeface(typeface2);

        search_form_bt.setOnClickListener(this);

        List<String> spinnerList = new ArrayList<String>();
        spinnerList.add("--Select--");
        spinnerList.add("Previous Id Card Number");
        spinnerList.add("Online Registered Id");
        spinnerList.add("Mobile Number");
        spinnerList.add("Name");
        spinnerList.add("Aadhar Number");
        spinnerList.add("Email Id");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,R.layout.myspinner_item, spinnerList);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        search_form_spinner.setAdapter(spinnerAdapter);

        search_form_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position !=0 )
                {
                    item = parent.getItemAtPosition(position).toString();
                    search_form_hs_ll.setVisibility(View.VISIBLE);
                    search_form_value_tv.setText(item);
                    if (position == 1 )
                    {
                        search_form_data_edt.setText("");
                        search_form_data_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(10)
                                });
                        falgpreviousID = 1;
                        flagOnlineRegisterID = 0;
                        flagMobileNumber = 0;
                        flagName = 0;
                        flagAAdhar = 0;
                        flagEmailId = 0;
                    }

                    if(position == 2 )
                    {
                        search_form_data_edt.setText("");

                        search_form_data_edt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(14)
                                });
                        falgpreviousID = 0;
                        flagOnlineRegisterID = 1;
                        flagMobileNumber = 0;
                        flagName = 0;
                        flagAAdhar = 0;
                        flagEmailId = 0;
                    }
                    if(position == 3)
                    {
                        search_form_data_edt.setText("");
                        search_form_data_edt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(10)
                                });
                        falgpreviousID = 0;
                        flagOnlineRegisterID = 0;
                        flagMobileNumber = 1;
                        flagName = 0;
                        flagAAdhar = 0;
                        flagEmailId = 0;

                    }


                    if (position == 4 )
                    {
                        search_form_data_edt.setText("");
                        search_form_data_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(50)
                                });

                        falgpreviousID = 0;
                        flagOnlineRegisterID = 0;
                        flagMobileNumber = 0;
                        flagName = 1;
                        flagAAdhar = 0;
                        flagEmailId = 0;
                    }

                    if(position == 5)
                    {
                        search_form_data_edt.setText("");

                        search_form_data_edt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(12)
                                });
                        falgpreviousID = 0;
                        flagOnlineRegisterID = 0;
                        flagMobileNumber = 0;
                        flagName = 0;
                        flagAAdhar = 1;
                        flagEmailId = 0;
                    }




                    if(position == 6)
                    {
                        search_form_data_edt.setText("");
                        search_form_data_edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        search_form_data_edt.setFilters(new InputFilter[]
                                {
                                        new InputFilter.LengthFilter(30)
                                });
                        falgpreviousID = 0;
                        flagOnlineRegisterID = 0;
                        flagMobileNumber = 0;
                        flagName = 0;
                        flagAAdhar = 0;
                        flagEmailId = 1;
                    }

                }
                else
                {
                    search_form_hs_ll.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    @Override
    public void onClick(View v)
    {

        if (v == search_form_dob_txt)
        {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme,new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    String strMonth = null,strDay = null;
                    int month = monthOfYear+1;
                    if(month < 10){

                        strMonth = "0" + month;
                    }
                    else {
                        strMonth = month+"";
                    }
                    if(dayOfMonth < 10){

                        strDay  = "0" + dayOfMonth ;
                    }
                    else {
                        strDay = dayOfMonth+"";
                    }
                    stringDOB = String.valueOf(strDay + " - " + strMonth + " - " + year);
                    search_form_dob_txt.setText(stringDOB);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if(v == search_form_bt)
        {
            //if(falgpreviousID == 1 || flagName == 1 || flagOnlineRegisterID == 1 || flagMobileNumber == 1 || flagAAdhar == 1 || flagEmailId == 1) {
            if(search_form_spinner.getSelectedItemPosition() > 0)
            {
                if(stringDOB != null && !stringDOB.isEmpty())
                {
                    String ed_text = search_form_data_edt.getText().toString().trim();

                    if (search_form_data_edt.getText().toString().length() != 0) {
                        //

                        if(falgpreviousID == 1)
                        {
                            if(isPreviousIdCardNumber())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid PreviousIdCardNumber", Toast.LENGTH_SHORT).show();
                            }
                        }


                        if(flagOnlineRegisterID == 1)
                        {
                            if(isOnlineRegisteredId())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid OnlineRegisterID", Toast.LENGTH_SHORT).show();
                            }
                        }

                        if(flagMobileNumber == 1)
                        {
                            if(isMobileNumber())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
                            }
                        }


                        if(flagName == 1)
                        {
                            if(isName())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid Name", Toast.LENGTH_SHORT).show();
                            }
                        }



                        if(flagAAdhar == 1)
                        {
                            if(isAAdharNumber())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid AAdhar Number", Toast.LENGTH_SHORT).show();
                            }
                        }



                        if(flagEmailId == 1)
                        {
                            if(isEmailId())
                            {
                                Log.d("Purushotham", "TESTONE");
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Invalid Mail Id", Toast.LENGTH_SHORT).show();
                            }
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Please Provide details..!", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Select Date of birth", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getApplicationContext(), "Select Type", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private boolean isEmailId() {
        String emailId = search_form_data_edt.getText().toString();
        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        boolean result;


        if (!emailId.matches(EMAIL_REGEX)) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    private boolean isAAdharNumber() {
        String aadharNumber = search_form_data_edt.getText().toString();


        boolean result;

        String PreviousIdCardNumber = "^[0-9]+";
        if (!aadharNumber.matches(PreviousIdCardNumber) || aadharNumber.length() != 12) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    private boolean isMobileNumber() {
        String mobileNumber = search_form_data_edt.getText().toString();


        boolean result;

        String PreviousIdCardNumber = "^[0-9]+";
        if (!mobileNumber.matches(PreviousIdCardNumber) || mobileNumber.length() != 10) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    private boolean isOnlineRegisteredId() {
        String onlineRegisterNumber = search_form_data_edt.getText().toString();


        boolean result;

        String PreviousIdCardNumber = "^[0-9]+";
        if (!onlineRegisterNumber.matches(PreviousIdCardNumber) || onlineRegisterNumber.length() != 14) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    private boolean isName() {
        String name = search_form_data_edt.getText().toString();


        boolean result;

        String name_val = "^[a-zA-Z]+";
        if (!name.matches(name_val) || name.length() <  3) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    private boolean isPreviousIdCardNumber() {
        String phone = search_form_data_edt.getText().toString();
        boolean result;
        String PreviousIdCardNumber = "^[a-zA-Z0-9]+";
        if (!phone.matches(PreviousIdCardNumber) || phone.length() != 10) {
            // update_phone_til.setError(getString(R.string.invalidNumber));
            result = false;
        }
        else {
            result = true;
        }
        return result;
    }

    /*private boolean validate() {


        return false;
    }*/
}
