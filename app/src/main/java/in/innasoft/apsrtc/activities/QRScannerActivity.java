package in.innasoft.apsrtc.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture;
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import in.innasoft.apsrtc.MainActivity;
import in.innasoft.apsrtc.R;
import info.androidhive.barcode.BarcodeReader;
import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever;

public class QRScannerActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    private static final String TAG = "QRScannerActivity";
    BarcodeReader barcodeReader;
    EditText etUniqueId;
    Button submit_btn;
    RelativeLayout scanLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);


        if (isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            Log.e(TAG, "initActivityScreenOrientPortrait: " + "SCREEN_ORIENTATION_LANDSCAPE");
        }
        else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            Log.e(TAG, "initActivityScreenOrientPortrait: " + "SCREEN_POTRIENT");
        }




        // get the barcode reader instance
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode);
        etUniqueId = findViewById(R.id.uniqueId);
        submit_btn = findViewById(R.id.submit_btn);
        scanLayout = findViewById(R.id.scanLayout);

        etUniqueId.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                view.setFocusable(true);
                scanLayout.setVisibility(View.GONE);
                submit_btn.setVisibility(View.VISIBLE);
                return false;
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!etUniqueId.toString().isEmpty()) {
                    Intent intent = new Intent(QRScannerActivity.this, ValidateSuccessActivity.class);
                    intent.putExtra("code", "");
                    startActivity(intent);
                } else {
                    Toast.makeText(QRScannerActivity.this, "Please Enter Valid id", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public boolean isTablet() {
        return (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
    @Override
    public void onScanned(Barcode barcode) {

        // playing barcode reader beep sound
        barcodeReader.playBeep();

        // ticket details activity by passing barcode
        Intent intent = new Intent(QRScannerActivity.this, ValidateSuccessActivity.class);
        intent.putExtra("code", barcode.displayValue);
        startActivity(intent);

        Log.e("DATA", "" + barcode.displayValue);
    }

    @Override
    public void onScannedMultiple(List<Barcode> list) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String s) {
        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {
        finish();
    }


}
