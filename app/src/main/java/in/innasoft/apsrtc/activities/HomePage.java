package in.innasoft.apsrtc.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.innasoft.apsrtc.Adapter.HomeAdapter;
import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.models.HomeModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HomePage extends AppCompatActivity {
    Typeface typeface, typeface2;
    Button pass_above_ssc_bt, pass_below_ssc_bt, update_details_bt, clickhere_acdimicbt, demo_registration_bt;
    RecyclerView recyclerHome;
    HomeAdapter adapter;
    private List<HomeModel> homeModelList = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);


        Calendar calendar = Calendar.getInstance();
        int currentyear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH);
        Log.d("CURRENT MONTH", currentMonth + 1 + "");
        int previousYear = currentyear - 1;



        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");

        //opensansregular.ttf
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        recyclerHome = findViewById(R.id.recyclerHome);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerHome.setLayoutManager(mLayoutManager);
        recyclerHome.setItemAnimator(new DefaultItemAnimator());
        adapter = new HomeAdapter(getApplicationContext(),homeModelList);
        recyclerHome.setAdapter(adapter);

        homeData();

    }

    private void homeData() {

        HomeModel homeModel = new HomeModel("Registered Students", "Last Academic Year (2018-2019)", R.drawable.ic_rigisterdstudents);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Above SSC", "Student Pass", R.drawable.ic_above_ssc);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Below SSC", "Student Pass", R.drawable.ic_below_ssc);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Validate Pass", "Click here", R.drawable.ic_qr_code);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Update Details", "Click here", R.drawable.ic_refresh_button);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("My Bus Pass", "Click here", R.drawable.ic_busspass);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Demo Registration", "Click here", R.drawable.ic_demo_registartion);
        homeModelList.add(homeModel);

        homeModel = new HomeModel("Instructions", "Click here", R.drawable.ic_instructions);
        homeModelList.add(homeModel);

        adapter.notifyDataSetChanged();


    }

}
