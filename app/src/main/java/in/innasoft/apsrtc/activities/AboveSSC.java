package in.innasoft.apsrtc.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.models.CourseDurationModel;
import in.innasoft.apsrtc.models.CourseNameModel;
import in.innasoft.apsrtc.models.DistrictsModel;
import in.innasoft.apsrtc.models.DistrictsModelTwo;
import in.innasoft.apsrtc.models.FromDistrictModel;
import in.innasoft.apsrtc.models.InstAddressModel;
import in.innasoft.apsrtc.models.InstitutionNameModel;
import in.innasoft.apsrtc.models.MandalsModel;
import in.innasoft.apsrtc.models.MandalsModelTwo;
import in.innasoft.apsrtc.models.PassTypeModel;
import in.innasoft.apsrtc.models.RouteCenterModel;
import in.innasoft.apsrtc.models.RoutsViaModel;
import in.innasoft.apsrtc.models.ToDistrictModel;
import in.innasoft.apsrtc.models.VillagesModel;
import in.innasoft.apsrtc.utilities.AppUrls;
import in.innasoft.apsrtc.utilities.Config;
import in.innasoft.apsrtc.utilities.FilePath;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class AboveSSC extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    /*ID's*/
    ImageView gotoHome;
    Typeface typeface,typeface2;
    TextView welcome_headding_tv,buttom_com_tv,student10details_tv,studentdetails_tv,residentialaddress_tv,
            institutiondetails_tv,rootdetails_tv,agecal_tv,gender_tv,isemployee_tv,photo_tv,dob_txt,age_tv,institute_address_txt,above_ssc_dist_txt;
    EditText year_of_pass_et,scc_hall_ticket_number_et,name_et,father_guardener_name_et,aadhar_number_et,
            phone_number_et,email_id_et,addmission_number_et,staff_id_et,address_et,above_ssc_from_act,above_ssc_to_act;
    Button choose_gallery_bt,take_photo_bt,submit_bt;
    String value;

    TextInputLayout year_of_pass_til,scc_hall_ticket_number_til,name_til,father_guardener_name_til,aadhar_number_til,
            phone_number_til,email_id_til,addmission_number_til,staff_id_til,address_til;

    RadioGroup gender_group,isemployee_group;
    RadioButton male_radio,female_radio,isemp_yes_radio,isemp_no_radio;
    ImageView profile_image_iv;

    Spinner above_ssc_spinner1,above_ssc_spinner2,above_ssc_spinner3,above_ssc_spinner4,above_ssc_spinner5,above_ssc_spinner6,above_ssc_spinner7
            ,above_ssc_spinner8,above_ssc_spinner9,above_ssc_spinner10,above_ssc_spinner11,above_ssc_spinner12,above_ssc_spinner13,
            above_ssc_spinner14,above_ssc_spinner15;

    ArrayAdapter<String> spinnerArrayAdapterDistricts,spinnerArrayAdapterMandals,spinnerArrayAdapterVillages;
    ArrayAdapter<String> spinnerArrayAdapterDist,spinnerArrayAdapterMnd;

    ArrayAdapter<String> spinnerArrayAdapterInstitute;
    ArrayAdapter<String> spinnerArrayAdapterCourseName;
    ArrayAdapter<String> spinnerArrayAdapterInstAddress;
    ArrayAdapter<String> spinnerArrayAdapterCourseDuration;
    ArrayAdapter<String> spinnerArrayAdapterCRouteCenter;

    ArrayAdapter<String> spinnerArrayAdapterFromRouteDistrict;
    ArrayAdapter<String> spinnerArrayAdapterToRouteDistrict;
    ArrayAdapter<String> spinnerArrayAdapterToRouteVia;

    ArrayAdapter<String> spinnerArrayAdapterPassType;
    private int mYear, mMonth, mDay;

    AutoCompleteTextView fromAutoCompleteTextView,toAutoCompleteTextView;
    ArrayAdapter<String> fromAutoCompleteAdapter,toAutoCompleteAdapter;


    /*Variables*/

    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;

    private static final String TAG = AboveSSC.class.getSimpleName();
    private Uri fileUri;
    private String selectedFilePath;

    ArrayList<DistrictsModel> districtsModels,districtsModel2;
    ArrayList<MandalsModel> mandalsModels,mandalsModel2;
    ArrayList<VillagesModel> villagesModels,villagesModel2;
    ArrayList<String> districtsString,mandalsString,villagesString;

    ArrayList<DistrictsModelTwo> instDistrictsModels,instDistrictsModel2;
    ArrayList<MandalsModelTwo> instMandalsModels,instMandalsModel2;
    ArrayList<String> instDistrictsString,instMandalsString;

    ArrayList<InstitutionNameModel> institutionNameModels,institutionNameModel2;
    ArrayList<String> institutionNameString;

    ArrayList<CourseNameModel> courseNameModels,courseNameModel2;
    ArrayList<String> courseNameString;

    ArrayList<InstAddressModel> instAddressModels,instAddressModel2;
    ArrayList<String> instAddressString;

    ArrayList<CourseDurationModel> courseDurationModels,courseDurationModel2;
    ArrayList<String> courseDurationString;




    ArrayList<PassTypeModel> passTypeModels,passTypeModel2;
    ArrayList<String> passTypeString;


    ArrayList<RouteCenterModel> routeCenterModels,routeCenterModel2;
    ArrayList<String> routeCenterString;


    ArrayList<FromDistrictModel> formrouteDistrictModels,formrouteDistrictModels2;
    ArrayList<String> formrouteDistrictModelsString;

    ArrayList<ToDistrictModel> torouteDistrictModels,torouteDistrictModels2;
    ArrayList<String> torouteDistrictModelsString;


    ArrayList<RoutsViaModel> routsViaModels,routsViaModels2;
    ArrayList<String> routsViaModelsString;

    ArrayList<String> fromStopsArrayList = new ArrayList<String>();
    ArrayList<String> toStopsArrayList = new ArrayList<String>();

    Map<String, String> fromStopsID = new HashMap<String, String>();
    Map<String, String> toStopsID = new HashMap<String, String>();

    String from_stop_id,to_stop_id;

    String gender_string,is_employee_string;
    String date_of_birth_string,age_string;
    String ssc_board_type_string,ssc_pass_type_string;
    String residential_district_id_string,residential_mandal_id_string,residential_village_id_string;
    String institution_district_id_string,institution_mandal_id_string,institution_id_string,course_id_string,present_course_year_id_string,institution_address_id_string;
    String route_district_id_string,center_id_string,pass_type_id_string,from_place_district_id_string,to_place_district_id_string,from_place_id_string,to_place_id_string,via_place_id_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_above_ssc);

        above_ssc_from_act = (EditText) findViewById(R.id.above_ssc_from_act);
//        fromAutoCompleteTextView.setThreshold(1);
        above_ssc_to_act = (EditText) findViewById(R.id.above_ssc_to_act);
//        above_ssc_to_act.setThreshold(1);

        districtsModels = new ArrayList<DistrictsModel>();
        districtsModel2 = new ArrayList<DistrictsModel>();
        districtsString = new ArrayList<String>();
        districtsString.add("Select District");


        instDistrictsModels = new ArrayList<DistrictsModelTwo>();
        instDistrictsModel2 = new ArrayList<DistrictsModelTwo>();
        instDistrictsString = new ArrayList<String>();
        instDistrictsString.add("Select District");


        institutionNameModels = new ArrayList<InstitutionNameModel>();
        institutionNameModel2 = new ArrayList<InstitutionNameModel>();
        institutionNameString = new ArrayList<String>();
        institutionNameString.add("Select Institution Name");



        mandalsModels = new ArrayList<MandalsModel>();
        mandalsModel2 = new ArrayList<MandalsModel>();
        mandalsString = new ArrayList<String>();
        mandalsString.add("Select Mandal");

        instMandalsModels = new ArrayList<MandalsModelTwo>();
        instMandalsModel2 = new ArrayList<MandalsModelTwo>();
        instMandalsString = new ArrayList<String>();
        instMandalsString.add("Select Mandal");

        courseNameModels = new ArrayList<CourseNameModel>();
        courseNameModel2 = new ArrayList<CourseNameModel>();
        courseNameString = new ArrayList<String>();
        courseNameString.add("Select Course");

        instAddressModels = new ArrayList<InstAddressModel>();
        instAddressModel2 = new ArrayList<InstAddressModel>();
        instAddressString = new ArrayList<String>();

        courseDurationModels = new ArrayList<CourseDurationModel>();
        courseDurationModel2 = new ArrayList<CourseDurationModel>();
        courseDurationString = new ArrayList<String>();
        courseDurationString.add("Select Duration");


        villagesModels = new ArrayList<VillagesModel>();
        villagesModel2 = new ArrayList<VillagesModel>();
        villagesString = new ArrayList<String>();
        villagesString.add("Select Village");





        routeCenterModels = new ArrayList<RouteCenterModel>();
        routeCenterModel2 = new ArrayList<RouteCenterModel>();
        routeCenterString = new ArrayList<String>();
        routeCenterString.add("Select Center");




        passTypeModels = new ArrayList<PassTypeModel>();
        passTypeModel2 = new ArrayList<PassTypeModel>();
        passTypeString = new ArrayList<String>();
        passTypeString.add("Select Pass type");




        formrouteDistrictModels = new ArrayList<FromDistrictModel>();
        formrouteDistrictModels2 = new ArrayList<FromDistrictModel>();
        formrouteDistrictModelsString = new ArrayList<String>();
        formrouteDistrictModelsString.add("Select From District");


        torouteDistrictModels = new ArrayList<ToDistrictModel>();
        torouteDistrictModels2 = new ArrayList<ToDistrictModel>();
        torouteDistrictModelsString = new ArrayList<String>();
        torouteDistrictModelsString.add("Select To District");



        routsViaModels = new ArrayList<RoutsViaModel>();
        routsViaModels2 = new ArrayList<RoutsViaModel>();
        routsViaModelsString = new ArrayList<String>();
        routsViaModelsString.add("Select");


        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");

        //opensansregular.ttf
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

//        welcome_headding_tv = (TextView) findViewById(R.id.welcome_headding_tv);
//        welcome_headding_tv.setTypeface(typeface);

        buttom_com_tv = (TextView) findViewById(R.id.buttom_com_tv);
        buttom_com_tv.setTypeface(typeface);

        student10details_tv = (TextView) findViewById(R.id.student10details_tv);
        student10details_tv.setTypeface(typeface2);

        studentdetails_tv = (TextView) findViewById(R.id.studentdetails_tv);
        studentdetails_tv.setTypeface(typeface2);

        residentialaddress_tv = (TextView) findViewById(R.id.residentialaddress_tv);
        residentialaddress_tv.setTypeface(typeface2);

        institutiondetails_tv = (TextView) findViewById(R.id.institutiondetails_tv);
        institutiondetails_tv.setTypeface(typeface2);

        rootdetails_tv = (TextView) findViewById(R.id.rootdetails_tv);
        rootdetails_tv.setTypeface(typeface2);

        agecal_tv = (TextView) findViewById(R.id.agecal_tv);
        agecal_tv.setTypeface(typeface2);

        age_tv = (TextView) findViewById(R.id.age_tv);
        age_tv.setTypeface(typeface2);

        gender_tv = (TextView) findViewById(R.id.gender_tv);
        gender_tv.setTypeface(typeface2);

        isemployee_tv = (TextView) findViewById(R.id.isemployee_tv);
        isemployee_tv.setTypeface(typeface2);

        photo_tv = (TextView) findViewById(R.id.photo_tv);
        photo_tv.setTypeface(typeface2);

        year_of_pass_et = (EditText) findViewById(R.id.year_of_pass_et);
        year_of_pass_et.setTypeface(typeface2);

        scc_hall_ticket_number_et = (EditText) findViewById(R.id.scc_hall_ticket_number_et);
        scc_hall_ticket_number_et.setTypeface(typeface2);

        dob_txt = (TextView) findViewById(R.id.dob_txt);
        dob_txt.setTypeface(typeface2);
        dob_txt.setOnClickListener(this);

        name_et = (EditText) findViewById(R.id.name_et);
        name_et.setTypeface(typeface2);

        father_guardener_name_et = (EditText) findViewById(R.id.father_guardener_name_et);
        father_guardener_name_et.setTypeface(typeface2);

        aadhar_number_et = (EditText) findViewById(R.id.aadhar_number_et);
        aadhar_number_et.setTypeface(typeface2);

        phone_number_et  = (EditText) findViewById(R.id.phone_number_et);
        phone_number_et.setTypeface(typeface2);

        staff_id_et  = (EditText) findViewById(R.id.staff_id_et);
        staff_id_et.setTypeface(typeface2);

        address_et = (EditText) findViewById(R.id.address_et);
        address_et.setTypeface(typeface2);

        email_id_et = (EditText) findViewById(R.id.email_id_et);
        email_id_et.setTypeface(typeface2);

        addmission_number_et = (EditText) findViewById(R.id.addmission_number_et);
        addmission_number_et.setTypeface(typeface2);

        institute_address_txt = (TextView) findViewById(R.id.institute_address_txt);
        institute_address_txt.setTypeface(typeface2);

        /*institute_address_et = (EditText) findViewById(R.id.institute_address_et);
        institute_address_et.setTypeface(typeface2);*/

        choose_gallery_bt = (Button) findViewById(R.id.choose_gallery_bt);
        choose_gallery_bt.setTypeface(typeface2);

        take_photo_bt = (Button) findViewById(R.id.take_photo_bt);
        take_photo_bt.setTypeface(typeface2);

        submit_bt = (Button) findViewById(R.id.submit_bt);
        submit_bt.setTypeface(typeface2);
        submit_bt.setOnClickListener(this);

//        year_of_pass_til = (TextInputLayout) findViewById(R.id.year_of_pass_til);
//        year_of_pass_til.setTypeface(typeface2);
//
//        scc_hall_ticket_number_til = (TextInputLayout) findViewById(R.id.scc_hall_ticket_number_til);
//        scc_hall_ticket_number_til.setTypeface(typeface2);

//        address_til = (TextInputLayout) findViewById(R.id.address_til);
//        address_til.setTypeface(typeface2);

//        name_til = (TextInputLayout) findViewById(R.id.name_til);
//        name_til.setTypeface(typeface2);
//
//        father_guardener_name_til = (TextInputLayout) findViewById(R.id.father_guardener_name_til);
//        father_guardener_name_til.setTypeface(typeface2);

//        aadhar_number_til = (TextInputLayout) findViewById(R.id.aadhar_number_til);
//        aadhar_number_til.setTypeface(typeface2);
//
//        phone_number_til = (TextInputLayout) findViewById(R.id.phone_number_til);
//        phone_number_til.setTypeface(typeface2);
//
//        staff_id_til = (TextInputLayout) findViewById(R.id.staff_id_til);
//        staff_id_til.setTypeface(typeface2);
//
//        email_id_til = (TextInputLayout) findViewById(R.id.email_id_til);
//        email_id_til.setTypeface(typeface2);
//
//        addmission_number_til = (TextInputLayout) findViewById(R.id.addmission_number_til);
//        addmission_number_til.setTypeface(typeface2);

        /*institute_address_til = (TextInputLayout) findViewById(R.id.institute_address_til);
        institute_address_til.setTypeface(typeface2);*/

        gender_group = (RadioGroup) findViewById(R.id.gender_group);
        isemployee_group = (RadioGroup) findViewById(R.id.isemployee_group);

        male_radio = (RadioButton) findViewById(R.id.male_radio);
        male_radio.setTypeface(typeface2);


        female_radio = (RadioButton) findViewById(R.id.female_radio);
        female_radio.setTypeface(typeface2);


        isemp_yes_radio = (RadioButton) findViewById(R.id.isemp_yes_radio);
        isemp_yes_radio.setTypeface(typeface2);


        isemp_no_radio = (RadioButton) findViewById(R.id.isemp_no_radio);
        isemp_no_radio.setChecked(true);
        isemp_no_radio.setTypeface(typeface2);


        above_ssc_dist_txt = (TextView) findViewById(R.id.above_ssc_dist_txt);
        above_ssc_dist_txt.setTypeface(typeface2);

        profile_image_iv = (ImageView) findViewById(R.id.profile_image_iv);

        gotoHome = (ImageView) findViewById(R.id.gotoHome);

        gotoHome.setOnClickListener(this);
        choose_gallery_bt.setOnClickListener(this);
        take_photo_bt.setOnClickListener(this);


        //findInstAddress(address);


        isemployee_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                if(rb.getText().equals("No"))
                {
                    staff_id_til.setVisibility(View.GONE);
                    staff_id_et.setVisibility(View.GONE);
                }else {
//                    staff_id_til.setVisibility(View.VISIBLE);
                    staff_id_et.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        above_ssc_spinner1 = (Spinner) findViewById(R.id.above_ssc_spinner1);

        List<String> spinnerList1 = new ArrayList<String>();
        spinnerList1.add("--Select--");
        spinnerList1.add("AP SSC");
        spinnerList1.add("CBSC");
        spinnerList1.add("ICSE");
        spinnerList1.add("ORIENTAL");
        spinnerList1.add("APOS");
        spinnerList1.add("Other Board");

        ArrayAdapter<String> spinnerAdapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, spinnerList1);

        spinnerAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        above_ssc_spinner1.setAdapter(spinnerAdapter1);

        above_ssc_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position !=0 )
                {
                    String item = parent.getItemAtPosition(position).toString();
                    ssc_board_type_string = item;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        above_ssc_spinner2 = (Spinner) findViewById(R.id.above_ssc_spinner2);

        List<String> spinnerList2 = new ArrayList<String>();
        spinnerList2.add("--Select--");
        spinnerList2.add("Regular");
        spinnerList2.add("Supplementary");

        ArrayAdapter<String> spinnerAdapter2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, spinnerList2);

        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        above_ssc_spinner2.setAdapter(spinnerAdapter2);

        above_ssc_spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position !=0 )
                {
                    String item = parent.getItemAtPosition(position).toString();
                    ssc_pass_type_string = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        above_ssc_spinner3 = (Spinner) findViewById(R.id.above_ssc_spinner3);
        above_ssc_spinner4 = (Spinner) findViewById(R.id.above_ssc_spinner4);
        above_ssc_spinner5 = (Spinner) findViewById(R.id.above_ssc_spinner5);
        above_ssc_spinner6 = (Spinner) findViewById(R.id.above_ssc_spinner6);
        above_ssc_spinner7 = (Spinner) findViewById(R.id.above_ssc_spinner7);
        above_ssc_spinner8 = (Spinner) findViewById(R.id.above_ssc_spinner8);
        above_ssc_spinner9 = (Spinner) findViewById(R.id.above_ssc_spinner9);
        above_ssc_spinner10 = (Spinner) findViewById(R.id.above_ssc_spinner10);
        above_ssc_spinner11 = (Spinner) findViewById(R.id.above_ssc_spinner11);
        above_ssc_spinner12 = (Spinner) findViewById(R.id.above_ssc_spinner12);

        above_ssc_spinner13 = (Spinner) findViewById(R.id.above_ssc_spinner13);
        above_ssc_spinner14 = (Spinner) findViewById(R.id.above_ssc_spinner14);
        above_ssc_spinner15 = (Spinner) findViewById(R.id.above_ssc_spinner15);



        spinnerArrayAdapterDistricts = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, districtsString);
        spinnerArrayAdapterDistricts.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner3.setAdapter(spinnerArrayAdapterMandals);



        spinnerArrayAdapterMandals = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, mandalsString);
        spinnerArrayAdapterMandals.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner4.setAdapter(spinnerArrayAdapterDistricts);


        spinnerArrayAdapterVillages = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, villagesString);
        spinnerArrayAdapterVillages.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner5.setAdapter(spinnerArrayAdapterVillages);



        spinnerArrayAdapterDist = new ArrayAdapter<String>(this, R.layout.spinner_item_all_caps,instDistrictsString);
        spinnerArrayAdapterDist.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner6.setAdapter(spinnerArrayAdapterDist);

        spinnerArrayAdapterMnd = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, instMandalsString);
        spinnerArrayAdapterMnd.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner7.setAdapter(spinnerArrayAdapterMnd);

        spinnerArrayAdapterInstitute = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, institutionNameString);
        spinnerArrayAdapterInstitute.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner8.setAdapter(spinnerArrayAdapterInstitute);

        spinnerArrayAdapterCourseName = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, courseNameString);
        spinnerArrayAdapterCourseName.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner9.setAdapter(spinnerArrayAdapterCourseName);

       /* spinnerArrayAdapterInstAddress = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, instAddressString);
        spinnerArrayAdapterInstAddress.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner9.setAdapter(spinnerArrayAdapterInstAddress);*/

        spinnerArrayAdapterCourseDuration = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, courseDurationString);
        spinnerArrayAdapterCourseDuration.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner10.setAdapter(spinnerArrayAdapterCourseDuration);



        spinnerArrayAdapterCRouteCenter = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, routeCenterString);
        spinnerArrayAdapterCRouteCenter.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner11.setAdapter(spinnerArrayAdapterCRouteCenter);



        spinnerArrayAdapterPassType = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, passTypeString);
        spinnerArrayAdapterPassType.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner12.setAdapter(spinnerArrayAdapterPassType);


        spinnerArrayAdapterFromRouteDistrict = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, formrouteDistrictModelsString);
        spinnerArrayAdapterFromRouteDistrict.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner13.setAdapter(spinnerArrayAdapterFromRouteDistrict);



        spinnerArrayAdapterToRouteDistrict = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, torouteDistrictModelsString);
        spinnerArrayAdapterToRouteDistrict.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner14.setAdapter(spinnerArrayAdapterToRouteDistrict);


        spinnerArrayAdapterToRouteVia = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, routsViaModelsString);
        spinnerArrayAdapterToRouteVia.setDropDownViewResource(R.layout.spinner_item_all_caps);
        above_ssc_spinner15.setAdapter(spinnerArrayAdapterToRouteVia);

        final StringRequest stringRequest2 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            formrouteDistrictModelsString.clear();
                            formrouteDistrictModels.clear();
                            formrouteDistrictModels2.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            formrouteDistrictModelsString.add("--Select Form District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                FromDistrictModel dm = new FromDistrictModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                formrouteDistrictModels.add(dm);
                                formrouteDistrictModels2.add(dm);
                                formrouteDistrictModelsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            above_ssc_spinner13.setAdapter(spinnerArrayAdapterFromRouteDistrict);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue2 = Volley.newRequestQueue(getApplicationContext());
        requestQueue2.add(stringRequest2);



        final StringRequest stringRequest3 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            torouteDistrictModelsString.clear();
                            torouteDistrictModels.clear();
                            torouteDistrictModels2.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            torouteDistrictModelsString.add("--Select To District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                ToDistrictModel dm = new ToDistrictModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                torouteDistrictModels.add(dm);
                                torouteDistrictModels2.add(dm);
                                torouteDistrictModelsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            above_ssc_spinner14.setAdapter(spinnerArrayAdapterToRouteDistrict);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue3 = Volley.newRequestQueue(getApplicationContext());
        requestQueue3.add(stringRequest3);

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            districtsModels.clear();
                            districtsModel2.clear();
                            districtsString.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            districtsString.add("--Select District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                DistrictsModel dm = new DistrictsModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                districtsModels.add(dm);
                                districtsModel2.add(dm);
                                districtsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            above_ssc_spinner3.setAdapter(spinnerArrayAdapterDistricts);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);



        above_ssc_spinner3.setOnItemSelectedListener(this);
        above_ssc_spinner4.setOnItemSelectedListener(this);
        above_ssc_spinner5.setOnItemSelectedListener(this);


        final StringRequest stringRequest1 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            instDistrictsModels.clear();
                            instDistrictsModel2.clear();
                            instDistrictsString.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            instDistrictsString.add("--Select District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                DistrictsModelTwo dm = new DistrictsModelTwo();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                instDistrictsModels.add(dm);
                                instDistrictsModel2.add(dm);
                                instDistrictsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            above_ssc_spinner6.setAdapter(spinnerArrayAdapterDist);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        RequestQueue requestQueue1 = Volley.newRequestQueue(getApplicationContext());
        requestQueue1.add(stringRequest1);

        above_ssc_spinner6.setOnItemSelectedListener(this);
        above_ssc_spinner7.setOnItemSelectedListener(this);
        above_ssc_spinner8.setOnItemSelectedListener(this);
        above_ssc_spinner9.setOnItemSelectedListener(this);
        above_ssc_spinner10.setOnItemSelectedListener(this);
        above_ssc_spinner11.setOnItemSelectedListener(this);
        above_ssc_spinner12.setOnItemSelectedListener(this);
        above_ssc_spinner13.setOnItemSelectedListener(this);
        above_ssc_spinner14.setOnItemSelectedListener(this);
        above_ssc_spinner15.setOnItemSelectedListener(this);



        above_ssc_to_act.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        above_ssc_from_act.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

//        above_ssc_to_act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                from_stop_id = fromStopsID.get(fromAutoCompleteAdapter.getItem(position).toString());
//
//
//                from_place_id_string = from_stop_id;
//                /*Toast.makeText(getApplicationContext(),
//                        fromStopsID.get(fromAutoCompleteAdapter.getItem(position).toString()),
//                        Toast.LENGTH_SHORT).show();*/
//            }
//        });


//        toAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                to_stop_id = toStopsID.get(toAutoCompleteAdapter.getItem(position).toString());
//                to_place_id_string = to_stop_id;
//                findRoutsVia(from_stop_id, to_stop_id);
//               /* Toast.makeText(getApplicationContext(),
//                        from_stop_id+"\t"+to_stop_id,
//                        Toast.LENGTH_SHORT).show();*/
//            }
//        });


    }

    private void findRoutsVia(final String from_stop_id, final String to_stop_id) {
        routsViaModels2.clear();
        routsViaModels.clear();
        routsViaModelsString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_ROUTE_VIA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("GETTINGROUTVIA ", "GETTINGROUTVIA SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            routsViaModelsString.add("--Select--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                RoutsViaModel rvm = new RoutsViaModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("via");
                                rvm.setId(jsonObject1.getString("id"));
                                rvm.setVia(jsonObject1.getString("via"));
                                rvm.setKilometers(jsonObject1.getString("kilometers"));
                                rvm.setPrice(jsonObject1.getString("price"));
                                rvm.setId_charges(jsonObject1.getString("id_charges"));


                                routsViaModels.add(rvm);
                                routsViaModels2.add(rvm);
                                routsViaModelsString.add(jsonObject1.getString("via"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner15.setAdapter(spinnerArrayAdapterToRouteVia);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_village_id", from_stop_id);
                params.put("to_village_id", to_stop_id);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == dob_txt){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            String dob = String.valueOf(dayOfMonth+" - "+(monthOfYear+1)+" - "+year);

                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            Date birthDate = null;
                            try {
                                birthDate = sdf.parse(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);

                                String age = calculateAge(birthDate);

                                String segments[] = age.split(",");


                                String strMonth = null,strDay = null;
                                int month = monthOfYear+1;
                                if(month < 10){

                                    strMonth = "0" + month;
                                }
                                else {
                                    strMonth = month+"";
                                }
                                if(dayOfMonth < 10){

                                    strDay  = "0" + dayOfMonth ;
                                }
                                else {
                                    strDay = dayOfMonth+"";
                                }
                                date_of_birth_string = String.valueOf(strDay + " - " + strMonth + " - " + year);
                                dob_txt.setText(date_of_birth_string);
                                agecal_tv.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));
                                age_string = segments[0]+" years "+segments[1]+" months "+segments[2]+" days";
                                age_tv.setText("Your Age is : "+age_string);

                            } catch (ParseException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }


        if(v == gotoHome)
        {
            Intent intent = new Intent(getApplicationContext(), HomePage.class);
            startActivity(intent);
        }

        if(v == choose_gallery_bt)
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_REQUEST_IMAGE);

        }

        if(v == take_photo_bt)
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            File file=getOutputMediaFile(1);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
        }

        if (v == submit_bt)
        {

            if(validate())
            {
                if(selectedFilePath != null && !selectedFilePath.isEmpty())
                {
                    if(above_ssc_spinner1.getSelectedItemPosition() > 0 && above_ssc_spinner2.getSelectedItemPosition() > 0
                            && above_ssc_spinner3.getSelectedItemPosition() > 0 && above_ssc_spinner4.getSelectedItemPosition() > 0
                            && above_ssc_spinner5.getSelectedItemPosition() > 0 && above_ssc_spinner6.getSelectedItemPosition() > 0
                            && above_ssc_spinner7.getSelectedItemPosition() > 0 && above_ssc_spinner8.getSelectedItemPosition() > 0
                            && above_ssc_spinner9.getSelectedItemPosition() > 0 && above_ssc_spinner10.getSelectedItemPosition() > 0
                            && above_ssc_spinner11.getSelectedItemPosition() > 0 && above_ssc_spinner12.getSelectedItemPosition() > 0
                            && above_ssc_spinner13.getSelectedItemPosition() > 0 && above_ssc_spinner14.getSelectedItemPosition() > 0
                            && above_ssc_spinner15.getSelectedItemPosition() > 0)
                    {

                        if(male_radio.isChecked())
                        {
                            gender_string = "male";
                        }
                        if(female_radio.isChecked())
                        {
                            gender_string = "female";
                        }
                        if(isemp_yes_radio.isChecked())
                        {
                            is_employee_string = "yes";
                        }
                        if (isemp_no_radio.isChecked())
                        {
                            is_employee_string = "no";
                        }

                        if((gender_string != null && !gender_string.isEmpty()) && (is_employee_string != null && !is_employee_string.isEmpty()))
                        {
                            //Toast.makeText(getApplicationContext(), gender_string+"\t"+is_employee_string, Toast.LENGTH_SHORT).show();
                            String user_name = name_et.getText().toString();
                            String father_name = father_guardener_name_et.getText().toString();
                            String dob = date_of_birth_string;
                            String gender = gender_string;
                            String age = age_string;
                            String aadhar_no = aadhar_number_et.getText().toString();
                            String mobile = phone_number_et.getText().toString();
                            String email = email_id_et.getText().toString();
                            String photo = selectedFilePath;
                            String designation;
                            String staff_id;

                            if(is_employee_string.equals("yes"))
                            {
                                designation = "Employee";
                                staff_id = staff_id_et.getText().toString();
                            }
                            if(is_employee_string.equals("no"))
                            {
                                designation = "Student";
                                staff_id = "0";
                            }
                            String study_type = "above_ssc";

                            String residential_district_id,residential_mandal_id,residential_village_id,address;
                            residential_district_id = residential_district_id_string;
                            residential_mandal_id = residential_mandal_id_string;
                            residential_village_id = residential_village_id_string;
                            address = address_et.getText().toString();



                            String institution_district_id,institution_mandal_id,institution_id,course_id,present_course_year_id,admission_no,institution_address_id;
                            institution_district_id = institution_district_id_string;
                            institution_mandal_id = institution_mandal_id_string;
                            institution_id = institution_id_string;
                            course_id = course_id_string;
                            present_course_year_id = present_course_year_id_string;
                            admission_no = addmission_number_et.getText().toString();
                            institution_address_id = institution_address_id_string;




                            String route_district_id,center_id,pass_type_id,from_place_district_id,to_place_district_id,from_place_id,to_place_id,via_place_id;
                            route_district_id = route_district_id_string;
                            center_id = center_id_string;
                            pass_type_id = pass_type_id_string;
                            from_place_district_id = from_place_district_id_string;
                            to_place_district_id = to_place_district_id_string;
                            from_place_id = from_place_id_string;
                            to_place_id = to_place_id_string;
                            via_place_id = via_place_id_string;





                            String ssc_board_type,ssc_pass_type,ssc_year_of_pass,ssc_hall_ticket_no;
                            ssc_board_type = ssc_board_type_string;
                            ssc_pass_type = ssc_pass_type_string;
                            ssc_year_of_pass = year_of_pass_et.getText().toString();
                            ssc_hall_ticket_no = scc_hall_ticket_number_et.getText().toString();


                            Log.d("DISPLAYVALUES", "user_name\t"+user_name+"\nfather_name\t"+father_name+"\ndob\t"+dob+"\n"+"gender\t"+gender+"\n");
                            // registerAboveSSC()

                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Please Select Gender/Is Employ..!", Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Please provide all details..!", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please select Photo..!", Toast.LENGTH_SHORT).show();
                }

            }

        }

    }

    private boolean validate()
    {

        boolean result = true;

        String number_validation = "^[0-9]+";
        String email_validation = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";



        String email = email_id_et.getText().toString();
        if ( !email.matches(email_validation)) {
            email_id_til.setError(getString(R.string.invalid_email_address));
            result = false;
        }
        else {
            email_id_til.setErrorEnabled(false);
        }



        String address = address_et.getText().toString();
        if ( address.length() > 5) {
            address_til.setError(getString(R.string.invalid_address_address));
            result = false;
        }
        else {
            address_til.setErrorEnabled(false);
        }

        String yos = year_of_pass_et.getText().toString();
        if ( yos.length() != 4) {
            year_of_pass_til.setError(getString(R.string.invalid_yos));
            result = false;
        }
        else {
            year_of_pass_til.setErrorEnabled(false);
        }



        String hallticket = scc_hall_ticket_number_et.getText().toString();
        if (!(hallticket.matches(number_validation)) || (hallticket.length() != 12)) {
            scc_hall_ticket_number_til.setError(getString(R.string.invalid_hallticket));
            result = false;
        }
        else {
            scc_hall_ticket_number_til.setErrorEnabled(false);
        }



        String name = name_et.getText().toString();
        if ((name == null || name.length() < 3) && name != "^[a-zA-Z\\\\s]+") {

            name_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            name_til.setErrorEnabled(false);




        String fname = father_guardener_name_et.getText().toString();
        if ((fname == null || fname.length() < 3) && fname != "^[a-zA-Z\\\\s]+") {

            father_guardener_name_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            father_guardener_name_til.setErrorEnabled(false);





        String aadharNumber = aadhar_number_et.getText().toString();
        if (!aadharNumber.matches(number_validation) || aadharNumber.length() != 12) {
            aadhar_number_til.setError(getString(R.string.invalid_aadhar_number));
            result = false;
        }
        else {
            aadhar_number_til.setErrorEnabled(false);
        }



        String phoneNumber = phone_number_et.getText().toString();
        if (!phoneNumber.matches(number_validation) || phoneNumber.length() != 10) {
            phone_number_til.setError(getString(R.string.invalid_phone_number));
            result = false;
        }
        else {
            phone_number_til.setErrorEnabled(false);
        }




        String admissionNumber = addmission_number_et.getText().toString();
        if (!admissionNumber.matches(number_validation) || admissionNumber.length() != 15) {
            addmission_number_til.setError(getString(R.string.invalid_admision_number));
            result = false;
        }
        else {
            addmission_number_til.setErrorEnabled(false);
        }




        if(isemp_yes_radio.isChecked())
        {
            String staff_id = staff_id_et.getText().toString();
            if (staff_id.length() > 5) {
                staff_id_til.setError(getString(R.string.invalid_staff_number));
                result = false;
            }
            else {
                staff_id_til.setErrorEnabled(false);
            }
        }



        return result;
    }

    private String calculateAge(Date birthDate)
    {

        int years = 0;
        int months = 0;
        int days = 0;
        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        //Get difference between months
        months = currMonth - birthMonth;
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }

        return years+","+months+","+days;
    }


    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST_IMAGE)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {

        selectedFilePath = FilePath.getPath(this, fileUri);

        Log.i(TAG, "Selected File Path:" + selectedFilePath);

       /* Thread thread=new Thread(new Runnable(){
            public void run(){

            }
        });
        thread.start();*/

        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[24 * 1024];
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Bitmap bmp1 = BitmapFactory.decodeFile(selectedFilePath, options);
            Bitmap b1 = ThumbnailUtils.extractThumbnail(bmp1, 220, 220);
            profile_image_iv.setImageBitmap(b1);
            if (bmp1 != null) {
                bmp1.recycle();
            }

        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.i(TAG, "Selected File Path:" + selectedFilePath);


        Thread thread=new Thread(new Runnable(){
            public void run(){


            }
        });
        thread.start();

        String name = data.getData().getPath();
        Log.d("IMAGEPIC", name);

        profile_image_iv.setImageBitmap(bm);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;




        if(spinner.getId() == R.id.above_ssc_spinner3)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = districtsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < districtsModel2.size(); i++)
                {
                    findids.add(districtsModel2.get(i).getDistrict_name());
                }
                //Log.d("2225252KTEC", formrouteDistrictModels.get(position).getDistrict_name()+"");
                int selectionPosition= spinnerArrayAdapterFromRouteDistrict.getPosition(item);
                above_ssc_spinner13.setSelection(selectionPosition);
                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();
                // above_ssc_spinner13.setSelection(formrouteDistrictModels.get(position).getDistrict_name());
                residential_district_id_string = itemValue;
                findMandals(itemValue);
                findRouteCenter(itemValue);
            }

            else {
                above_ssc_spinner13.setSelection(0);

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }




        if(spinner.getId() == R.id.above_ssc_spinner4)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = mandalsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < mandalsModel2.size(); i++)
                {
                    findids.add(mandalsModel2.get(i).getMandal_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                residential_mandal_id_string = itemValue;
                findVillages(itemValue);
            }

            else {


            }
        }


        if(spinner.getId() == R.id.above_ssc_spinner5)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = villagesModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < villagesModel2.size(); i++)
                {
                    findids.add(villagesModel2.get(i).getVillage_name());
                }
                residential_village_id_string = itemValue;

            }

            else {


            }
        }


        if(spinner.getId() == R.id.above_ssc_spinner6)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = instDistrictsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < instDistrictsModel2.size(); i++)
                {
                    findids.add(instDistrictsModel2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                if(above_ssc_spinner3.getSelectedItemPosition() > 0)
                {
                    int mPossition = above_ssc_spinner3.getSelectedItemPosition();
                    String valueone = districtsModel2.get(mPossition-1).getId();
                    String finalString = valueone+","+itemValue;

                    findRouteCenter(finalString);
                }
                institution_district_id_string = itemValue;

                int selectionPosition= spinnerArrayAdapterToRouteDistrict.getPosition(item);
                above_ssc_spinner14.setSelection(selectionPosition);
                // above_ssc_spinner14.setSelection(formrouteDistrictModelsString.indexOf(itemValue));
                findInstMandals(itemValue);
            }

            else {
                above_ssc_spinner14.setSelection(0);
            }
        }

        if(spinner.getId() == R.id.above_ssc_spinner7)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = instMandalsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < instMandalsModel2.size(); i++)
                {
                    findids.add(instMandalsModel2.get(i).getMandal_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                institution_mandal_id_string = itemValue;

                findInstitutionName(itemValue);
            }

            else {

            }
        }

        if(spinner.getId() == R.id.above_ssc_spinner8)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = institutionNameModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < institutionNameModel2.size(); i++)
                {
                    findids.add(institutionNameModel2.get(i).getInstitution_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                institution_id_string = itemValue;
                findCourseName(itemValue);
                findInstAddress(itemValue);
            }

            else {

            }
        }

      /*  if(spinner.getId() == R.id.above_ssc_spinner8)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = institutionNameModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < institutionNameModel2.size(); i++)
                {
                    findids.add(institutionNameModel2.get(i).getInstitution_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                findInstAddress(itemValue);
            }

            else {

            }
        }*/

        if(spinner.getId() == R.id.above_ssc_spinner9)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = courseNameModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < courseNameModel2.size(); i++)
                {
                    findids.add(courseNameModel2.get(i).getInstitution_id());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                course_id_string = itemValue;

                findCourseDuration(itemValue);
            }

            else {

            }
        }


        if(spinner.getId() == R.id.above_ssc_spinner10)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = courseDurationModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < courseDurationModel2.size(); i++)
                {
                    findids.add(courseDurationModel2.get(i).getInstitution_id());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                present_course_year_id_string =itemValue;
            }

            else {

            }
        }



        if(spinner.getId() == R.id.above_ssc_spinner11)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = routeCenterModel2.get(position-1).getId();
                String itemValue2 = routeCenterModel2.get(position-1).getDistrict_id();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < routeCenterModel2.size(); i++)
                {
                    findids.add(routeCenterModel2.get(i).getCenter_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                center_id_string = itemValue;
                route_district_id_string = itemValue2;
                findPassType(itemValue);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }




        if(spinner.getId() == R.id.above_ssc_spinner12)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = passTypeModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < passTypeModel2.size(); i++)
                {
                    findids.add(passTypeModel2.get(i).getPass_type());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                pass_type_id_string = itemValue;

                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();
                //above_ssc_spinner13.setSelection(position-1);

                // findMandals(itemValue);
                //findRouteCenter(itemValue);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }


        if(spinner.getId() == R.id.above_ssc_spinner13)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = formrouteDistrictModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < formrouteDistrictModels2.size(); i++)
                {
                    findids.add(formrouteDistrictModels2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                findFromStops(itemValue);

                from_place_district_id_string = itemValue;
                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();
                //above_ssc_spinner13.setSelection(position-1);

                // findMandals(itemValue);
                //findRouteCenter(itemValue);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }
        if(spinner.getId() == R.id.above_ssc_spinner14)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = torouteDistrictModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < torouteDistrictModels2.size(); i++)
                {
                    findids.add(torouteDistrictModels2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                to_place_district_id_string = itemValue;
                findToStops(itemValue);
                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();
                //above_ssc_spinner14.setSelection(position-1);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }


        if(spinner.getId() == R.id.above_ssc_spinner15)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = routsViaModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < routsViaModels2.size(); i++)
                {
                    findids.add(routsViaModels2.get(i).getVia());
                }
                via_place_id_string = itemValue;
                long totalAmount = Long.parseLong(routsViaModels2.get(position-1).getPrice())+30;
                above_ssc_dist_txt.setText("Distance(in KMs):"+routsViaModels2.get(position-1).getKilometers()+" Pass Amount(₹):"+routsViaModels2.get(position-1).getPrice()+" Id Charges(₹): 30 /-"+" Total Amount(₹): "+totalAmount+" /-");
                // Log.d("2KTEC", item+"---"+itemValue);
                //findToStops(itemValue);
                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();
                //above_ssc_spinner14.setSelection(position-1);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }

    }

    private void findToStops(final String itemValue) {

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_TO_STOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            //passTypeString.add("--Select Pass type--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {


                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");

                                toStopsArrayList.add((jsonObject1.getString("village_name")).toUpperCase());
                                //fromStopsID.add(jsonObject1.getString("id"));
                                toStopsID.put((jsonObject1.getString("village_name")).toUpperCase(),jsonObject1.getString("id"));

                            }
                            String[] myArray = new String[toStopsArrayList.size()];
                            toStopsArrayList.toArray(myArray);
                            toAutoCompleteAdapter = new ArrayAdapter<String>
                                    (getApplicationContext(),R.layout.spinner_item_all_caps,myArray);

                            toAutoCompleteTextView.setAdapter(toAutoCompleteAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("to_district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findFromStops(final String itemValue) {


        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_FROM_STOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("FROMSTOPSFIND ", "FROMSTOPSFIND SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            //passTypeString.add("--Select Pass type--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {


                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");

                                fromStopsArrayList.add((jsonObject1.getString("village_name")).toUpperCase());
                                //fromStopsID.add(jsonObject1.getString("id"));
                                fromStopsID.put((jsonObject1.getString("village_name")).toUpperCase(),jsonObject1.getString("id"));

                            }
                            String[] myArray = new String[fromStopsArrayList.size()];
                            fromStopsArrayList.toArray(myArray);
                            fromAutoCompleteAdapter = new ArrayAdapter<String>
                                    (getApplicationContext(),R.layout.spinner_item_all_caps,myArray);



                            Log.d("DISPLAYDATA", ""+fromStopsID.toString());
                            fromAutoCompleteTextView.setAdapter(fromAutoCompleteAdapter);




                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findPassType(final String itemValue) {
        passTypeModel2.clear();
        passTypeModels.clear();
        passTypeString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_PASS_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "PassType SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            passTypeString.add("--Select Pass type--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                PassTypeModel pm = new PassTypeModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("pass_type");
                                pm.setId(jsonObject1.getString("id"));
                                pm.setCountry_id(jsonObject1.getString("country_id"));
                                pm.setState_id(jsonObject1.getString("state_id"));
                                pm.setDistrict_id(jsonObject1.getString("district_id"));
                                pm.setMandal_id(jsonObject1.getString("mandal_id"));
                                pm.setCenter_id(jsonObject1.getString("center_id"));
                                pm.setPass_type(jsonObject1.getString("pass_type"));
                                pm.setStatus(jsonObject1.getString("status"));

                                pm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                pm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                passTypeModels.add(pm);
                                passTypeModel2.add(pm);
                                passTypeString.add(jsonObject1.getString("pass_type"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner12.setAdapter(spinnerArrayAdapterPassType);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("center_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findRouteCenter(final String itemValue) {




        routeCenterModel2.clear();
        routeCenterModels.clear();
        routeCenterString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_CENTERS_IN_ROUTE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "Center SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            routeCenterString.add("--Select Center--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                RouteCenterModel rm = new RouteCenterModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("center");
                                rm.setId(jsonObject1.getString("id"));
                                rm.setCountry_id(jsonObject1.getString("country_id"));
                                rm.setState_id(jsonObject1.getString("state_id"));
                                rm.setDistrict_id(jsonObject1.getString("district_id"));
                                rm.setCenter_name(jsonObject1.getString("center_name"));
                                rm.setStatus(jsonObject1.getString("status"));
                                rm.setMandal_idl(jsonObject1.getString("mandal_id"));
                                rm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                rm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                routeCenterModels.add(rm);
                                routeCenterModel2.add(rm);
                                routeCenterString.add(jsonObject1.getString("center_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner11.setAdapter(spinnerArrayAdapterCRouteCenter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findCourseDuration(final String itemValue) {

        courseDurationString.clear();
        courseDurationModel2.clear();
        courseDurationModels.clear();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_COURSEDURATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("COURSEDURATION ", "SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            courseDurationString.add("--Select Duration--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                CourseDurationModel cdm = new CourseDurationModel();

                                Log.d("DURATION", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution_course_year");
                                cdm.setId(jsonObject1.getString("id"));
                                cdm.setCountry_id(jsonObject1.getString("country_id"));
                                cdm.setState_id(jsonObject1.getString("state_id"));
                                cdm.setDistrict_id(jsonObject1.getString("district_id"));
                                cdm.setMandal_id(jsonObject1.getString("mandal_id"));
                                cdm.setInstitution_id(jsonObject1.getString("institution_id"));
                                cdm.setInstitution_cource_id(jsonObject1.getString("institution_cource_id"));
                                cdm.setInstitutions_cource_years(jsonObject1.getString("institutions_cource_years"));
                                cdm.setStatus(jsonObject1.getString("status"));
                                cdm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                cdm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                courseDurationModels.add(cdm);
                                courseDurationModel2.add(cdm);
                                courseDurationString.add(jsonObject1.getString("institutions_cource_years"));
                            }

                            above_ssc_spinner10.setAdapter(spinnerArrayAdapterCourseDuration);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("institution_cource_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }




    private void findInstAddress(final String itemValue) {
        final StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_INSTADDRESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("ADDRESS", "SUCCESS" + response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            // instAddressString.add("--Select--");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                // InstAddressModel iam = new InstAddressModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution_address");

                                jsonObject1.getString("country_id");
                                jsonObject1.getString("state_id");
                                jsonObject1.getString("district_id");
                                jsonObject1.getString("mandal_id");
                                jsonObject1.getString("institution_id");
                                institution_address_id_string = jsonObject1.getString("id");
                                //String message = jsonObject1.getString("institution_address");
                                institute_address_txt.setText(jsonObject1.getString("institution_address"));
                                jsonObject1.getString("status");
                                jsonObject1.getString("created_date_time");
                                jsonObject1.getString("updated_date_time");

                                // instAddressModels.add(iam);
                                // instAddressModel2.add(iam);
                                // instAddressString.add(jsonObject1.getString("institution_address"));
                            }

                            //  above_ssc_spinner10.setAdapter(spinnerArrayAdapterInstAddress);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("institution_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue2 = Volley.newRequestQueue(getApplicationContext());
        requestQueue2.add(stringRequest2);

    }


    private void findCourseName(final String itemValue) {

        courseNameString.clear();
        courseNameModel2.clear();
        courseNameModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_COURSENAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Log.d("REGISTRATION ", "VILLAGES SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            courseNameString.add("--Select Course--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                CourseNameModel cnm = new CourseNameModel();

                                // Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution_course");
                                cnm.setId(jsonObject1.getString("id"));
                                cnm.setCountry_id(jsonObject1.getString("country_id"));
                                cnm.setState_id(jsonObject1.getString("state_id"));
                                cnm.setDistrict_id(jsonObject1.getString("district_id"));
                                cnm.setMandal_id(jsonObject1.getString("mandal_id"));
                                cnm.setInstitution_id(jsonObject1.getString("institution_id"));
                                cnm.setInstitution_cource_name(jsonObject1.getString("institution_cource_name"));
                                cnm.setStatus(jsonObject1.getString("status"));
                                cnm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                cnm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                courseNameModels.add(cnm);
                                courseNameModel2.add(cnm);
                                courseNameString.add(jsonObject1.getString("institution_cource_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner9.setAdapter(spinnerArrayAdapterCourseName);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("institution_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);


    }

    private void findInstitutionName(final String itemValue) {

        institutionNameString.clear();
        institutionNameModel2.clear();
        institutionNameModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_INSTITUTIONNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "VILLAGES SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            institutionNameString.add("--Select Institution Name--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                InstitutionNameModel inm = new InstitutionNameModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution");
                                inm.setId(jsonObject1.getString("id"));
                                inm.setInstitution_type(jsonObject1.getString("institution_type"));
                                inm.setCountry_id(jsonObject1.getString("country_id"));
                                inm.setState_id(jsonObject1.getString("state_id"));
                                inm.setDistrict_id(jsonObject1.getString("district_id"));
                                inm.setMandal_id(jsonObject1.getString("mandal_id"));
                                inm.setInstitution_name(jsonObject1.getString("institution_name"));
                                inm.setStatus(jsonObject1.getString("status"));
                                inm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                inm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                institutionNameModels.add(inm);
                                institutionNameModel2.add(inm);
                                institutionNameString.add(jsonObject1.getString("institution_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner8.setAdapter(spinnerArrayAdapterInstitute);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mandal_id", itemValue);
                params.put("institution_type","above_ssc");

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findVillages(final String itemValue) {
        villagesString.clear();
        villagesModel2.clear();
        villagesModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_VILLAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "VILLAGES SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            villagesString.add("--Select Village--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                VillagesModel vm = new VillagesModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");
                                vm.setId(jsonObject1.getString("id"));
                                vm.setCountry_id(jsonObject1.getString("country_id"));
                                vm.setState_id(jsonObject1.getString("state_id"));
                                vm.setDistrict_id(jsonObject1.getString("district_id"));
                                vm.setMandal_id(jsonObject1.getString("mandal_id"));
                                vm.setVillage_name(jsonObject1.getString("village_name"));
                                vm.setStatus(jsonObject1.getString("status"));
                                vm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                vm.setUpdate_date_time(jsonObject1.getString("update_date_time"));

                                villagesModels.add(vm);
                                villagesModel2.add(vm);
                                villagesString.add(jsonObject1.getString("village_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner5.setAdapter(spinnerArrayAdapterVillages);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mandal_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findMandals(final String itemValue) {
        mandalsString.clear();
        mandalsModel2.clear();
        mandalsModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_MANDALS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "DISTRICS SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            mandalsString.add("--Select Mandal--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                MandalsModel mm = new MandalsModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("mandal");
                                mm.setId(jsonObject1.getString("id"));
                                mm.setCountry_id(jsonObject1.getString("country_id"));
                                mm.setState_id(jsonObject1.getString("state_id"));
                                mm.setDistrict_id(jsonObject1.getString("district_id"));
                                mm.setMandal_name(jsonObject1.getString("mandal_name"));
                                mm.setStatus(jsonObject1.getString("status"));
                                mm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                mm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                mandalsModels.add(mm);
                                mandalsModel2.add(mm);
                                mandalsString.add(jsonObject1.getString("mandal_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner4.setAdapter(spinnerArrayAdapterMandals);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }



    private void findInstMandals(final String itemValue) {
        instMandalsString.clear();
        instMandalsModels.clear();
        instMandalsModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_MANDALS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "DISTRICS SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            instMandalsString.add("--Select Mandal--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                MandalsModelTwo mm = new MandalsModelTwo();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("mandal");
                                mm.setId(jsonObject1.getString("id"));
                                mm.setCountry_id(jsonObject1.getString("country_id"));
                                mm.setState_id(jsonObject1.getString("state_id"));
                                mm.setDistrict_id(jsonObject1.getString("district_id"));
                                mm.setMandal_name(jsonObject1.getString("mandal_name"));
                                mm.setStatus(jsonObject1.getString("status"));
                                mm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                mm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                instMandalsModels.add(mm);
                                instMandalsModel2.add(mm);
                                instMandalsString.add(jsonObject1.getString("mandal_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            above_ssc_spinner7.setAdapter(spinnerArrayAdapterMnd);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
