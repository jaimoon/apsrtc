package in.innasoft.apsrtc.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.utilities.Config;
import in.innasoft.apsrtc.utilities.FilePath;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class NGOForm extends AppCompatActivity implements View.OnClickListener {

    Typeface typeface,typeface2;
    TextView ngo_welcome_headding_tv,ngo_dob_txt,ngo_appointment_txt,ngo_retirement_txt,ngo_gender_txt,ngo_typeofpass_txt,ngo_photo_txt,ngo_or_txt
            ,ngo_buttom_com_tv;

    TextInputLayout ngo_name_til,ngo_designation_til,ngo_emp_id_til,ngo_department_til,ngo_fname_til,ngo_phone_number_til,ngo_scaleofpay_til,
            ngo_res_address_til,ngo_office_address_til;

    EditText ngo_name_edt,ngo_designation_edt,ngo_emp_id_edt,ngo_department_edt,ngo_fname_edt,ngo_phone_number_edt,ngo_scaleofpay_edt
            ,ngo_res_address_edt,ngo_office_address_edt;

    ImageView ngo_profile_pic_img;

    RadioGroup ngo_gender_id,ngo_typeofpass_id;

    RadioButton ngo_male_radio,ngo_female_radio,ngo_pass_ordinary_radio,ngo_pass_metro_radio;

    Button ngo_gallery_btn,ngo_take_btn,ngo_submit_btn;

    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;

    private static final String TAG = AboveSSC.class.getSimpleName();
    private Uri fileUri;
    private String selectedFilePath;

    private int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngoform);

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

//        ngo_welcome_headding_tv = (TextView) findViewById(R.id.ngo_welcome_headding_tv);
//        ngo_welcome_headding_tv.setTypeface(typeface);

        ngo_dob_txt = (TextView) findViewById(R.id.ngo_dob_txt);
        ngo_dob_txt.setTypeface(typeface2);
        ngo_dob_txt.setOnClickListener(this);

        ngo_appointment_txt = (TextView) findViewById(R.id.ngo_appointment_txt);
        ngo_appointment_txt.setTypeface(typeface2);
        ngo_appointment_txt.setOnClickListener(this);

        ngo_retirement_txt = (TextView) findViewById(R.id.ngo_retirement_txt);
        ngo_retirement_txt.setTypeface(typeface2);
        ngo_retirement_txt.setOnClickListener(this);

        ngo_gender_txt = (TextView) findViewById(R.id.ngo_gender_txt);
        ngo_gender_txt.setTypeface(typeface2);

        ngo_typeofpass_txt = (TextView) findViewById(R.id.ngo_typeofpass_txt);
        ngo_typeofpass_txt.setTypeface(typeface2);

        ngo_photo_txt = (TextView) findViewById(R.id.ngo_photo_txt);
        ngo_photo_txt.setTypeface(typeface2);

        ngo_or_txt = (TextView) findViewById(R.id.ngo_or_txt);
        ngo_or_txt.setTypeface(typeface2);

        ngo_buttom_com_tv = (TextView) findViewById(R.id.ngo_buttom_com_tv);
        ngo_buttom_com_tv.setTypeface(typeface);

        ngo_name_til = (TextInputLayout) findViewById(R.id.ngo_name_til);
        ngo_name_til.setTypeface(typeface2);

        ngo_designation_til = (TextInputLayout) findViewById(R.id.ngo_designation_til);
        ngo_designation_til.setTypeface(typeface2);

        ngo_emp_id_til = (TextInputLayout) findViewById(R.id.ngo_emp_id_til);
        ngo_emp_id_til.setTypeface(typeface2);

        ngo_department_til = (TextInputLayout) findViewById(R.id.ngo_department_til);
        ngo_department_til.setTypeface(typeface2);

        ngo_fname_til = (TextInputLayout) findViewById(R.id.ngo_fname_til);
        ngo_fname_til.setTypeface(typeface2);

        ngo_phone_number_til = (TextInputLayout) findViewById(R.id.ngo_phone_number_til);
        ngo_phone_number_til.setTypeface(typeface2);

        ngo_scaleofpay_til = (TextInputLayout) findViewById(R.id.ngo_scaleofpay_til);
        ngo_scaleofpay_til.setTypeface(typeface2);

        ngo_res_address_til = (TextInputLayout) findViewById(R.id.ngo_res_address_til);
        ngo_res_address_til.setTypeface(typeface2);

        ngo_office_address_til = (TextInputLayout) findViewById(R.id.ngo_office_address_til);
        ngo_office_address_til.setTypeface(typeface2);

        ngo_name_edt = (EditText) findViewById(R.id.ngo_name_edt);
        ngo_name_edt.setTypeface(typeface2);

        ngo_designation_edt = (EditText) findViewById(R.id.ngo_designation_edt);
        ngo_designation_edt.setTypeface(typeface2);

        ngo_emp_id_edt = (EditText) findViewById(R.id.ngo_emp_id_edt);
        ngo_emp_id_edt.setTypeface(typeface2);

        ngo_department_edt = (EditText) findViewById(R.id.ngo_department_edt);
        ngo_department_edt.setTypeface(typeface2);

        ngo_fname_edt = (EditText) findViewById(R.id.ngo_fname_edt);
        ngo_fname_edt.setTypeface(typeface2);

        ngo_phone_number_edt = (EditText) findViewById(R.id.ngo_phone_number_edt);
        ngo_phone_number_edt.setTypeface(typeface2);

        ngo_scaleofpay_edt = (EditText) findViewById(R.id.ngo_scaleofpay_edt);
        ngo_scaleofpay_edt.setTypeface(typeface2);

        ngo_res_address_edt = (EditText) findViewById(R.id.ngo_res_address_edt);
        ngo_res_address_edt.setTypeface(typeface2);

        ngo_office_address_edt = (EditText) findViewById(R.id.ngo_office_address_edt);
        ngo_office_address_edt.setTypeface(typeface2);

        ngo_profile_pic_img = (ImageView) findViewById(R.id.ngo_profile_pic_img);

        ngo_gender_id = (RadioGroup) findViewById(R.id.ngo_gender_id);

        ngo_typeofpass_id = (RadioGroup) findViewById(R.id.ngo_typeofpass_id);

        ngo_male_radio = (RadioButton) findViewById(R.id.ngo_male_radio);
        ngo_male_radio.setTypeface(typeface2);

        ngo_female_radio = (RadioButton) findViewById(R.id.ngo_female_radio);
        ngo_female_radio.setTypeface(typeface2);

        ngo_pass_ordinary_radio = (RadioButton) findViewById(R.id.ngo_pass_ordinary_radio);
        ngo_pass_ordinary_radio.setTypeface(typeface2);

        ngo_pass_metro_radio = (RadioButton) findViewById(R.id.ngo_pass_metro_radio);
        ngo_pass_metro_radio.setTypeface(typeface2);

        ngo_gallery_btn = (Button) findViewById(R.id.ngo_gallery_btn);
        ngo_gallery_btn.setTypeface(typeface2);
        ngo_gallery_btn.setOnClickListener(this);

        ngo_take_btn = (Button) findViewById(R.id.ngo_take_btn);
        ngo_take_btn.setTypeface(typeface2);
        ngo_take_btn.setOnClickListener(this);

        ngo_submit_btn = (Button) findViewById(R.id.ngo_submit_btn);
        ngo_submit_btn.setTypeface(typeface2);
        ngo_submit_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == ngo_gallery_btn){

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_REQUEST_IMAGE);

        }
        if (v == ngo_take_btn){

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            File file=getOutputMediaFile(1);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            startActivityForResult(intent, CAMERA_REQUEST_IMAGE);

        }
        if (v == ngo_dob_txt){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null,strDay = null;

                    int month = monthOfYear+1;
                    if(month < 10){

                        strMonth = "0" + month;
                    }
                    else {
                        strMonth = month+"";
                    }
                    if(dayOfMonth < 10){

                        strDay  = "0" + dayOfMonth ;
                    }
                    else {
                        strDay = dayOfMonth+"";
                    }
                    ngo_dob_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));

                }

            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == ngo_appointment_txt){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null,strDay = null;

                    int month = monthOfYear+1;
                    if(month < 10){

                        strMonth = "0" + month;
                    }
                    else {
                        strMonth = month+"";
                    }
                    if(dayOfMonth < 10){

                        strDay  = "0" + dayOfMonth ;
                    }
                    else {
                        strDay = dayOfMonth+"";
                    }
                    ngo_appointment_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));

                }

            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == ngo_retirement_txt){

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null,strDay = null;

                    int month = monthOfYear+1;
                    if(month < 10){

                        strMonth = "0" + month;
                    }
                    else {
                        strMonth = month+"";
                    }
                    if(dayOfMonth < 10){

                        strDay  = "0" + dayOfMonth ;
                    }
                    else {
                        strDay = dayOfMonth+"";
                    }
                    ngo_retirement_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));

                }

            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }


        if (v == ngo_submit_btn){

            validate();

        }

    }

    private boolean validate()
    {

        boolean result = true;

        String number_validation = "^[0-9]+";

        String name = ngo_name_edt.getText().toString();
        if ((name == null || name.length() < 3) && name != "^[a-zA-Z\\\\s]+") {

            ngo_name_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            ngo_name_til.setErrorEnabled(false);




        String fname = ngo_fname_edt.getText().toString();
        if ((fname == null || fname.length() < 3) && fname != "^[a-zA-Z\\\\s]+") {

            ngo_fname_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            ngo_fname_til.setErrorEnabled(false);

        String designation = ngo_designation_edt.getText().toString();
        if ((designation == null) && designation != "^[a-zA-Z\\\\s]+") {

            ngo_designation_til.setError(getString(R.string.invalid_designation));
            result = false;
        }
        else
            ngo_designation_til.setErrorEnabled(false);

        String department = ngo_department_edt.getText().toString();
        if ((department == null) && department != "^[a-zA-Z\\\\s]+") {

            ngo_department_til.setError(getString(R.string.invalid_department));
            result = false;
        }
        else
            ngo_department_til.setErrorEnabled(false);


        String phoneNumber = ngo_phone_number_edt.getText().toString();
        if (!phoneNumber.matches(number_validation) || phoneNumber.length() != 10) {
            ngo_phone_number_til.setError(getString(R.string.invalid_phone_number));
            result = false;
        }
        else {
            ngo_phone_number_til.setErrorEnabled(false);
        }



        return result;
    }

    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }


    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST_IMAGE)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {

        selectedFilePath = FilePath.getPath(this, fileUri);

        Log.i(TAG, "Selected File Path:" + selectedFilePath);

        Thread thread=new Thread(new Runnable(){
            public void run(){

            }
        });
        thread.start();

        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[24 * 1024];
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Bitmap bmp1 = BitmapFactory.decodeFile(selectedFilePath, options);
            Bitmap b1 = ThumbnailUtils.extractThumbnail(bmp1, 220, 220);
            ngo_profile_pic_img.setImageBitmap(b1);
            if (bmp1 != null) {
                bmp1.recycle();
            }

        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.i(TAG, "Selected File Path:" + selectedFilePath);


        Thread thread=new Thread(new Runnable(){
            public void run(){


            }
        });
        thread.start();

        String name = data.getData().getPath();
        Log.d("IMAGEPIC", name);

        ngo_profile_pic_img.setImageBitmap(bm);
    }
}
