package in.innasoft.apsrtc.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.models.BelowClassNameModel;
import in.innasoft.apsrtc.models.BelowDistrictsModel;
import in.innasoft.apsrtc.models.BelowDistrictsModelTwo;
import in.innasoft.apsrtc.models.BelowFromDistrictModel;
import in.innasoft.apsrtc.models.BelowMandalsModel;
import in.innasoft.apsrtc.models.BelowMandalsModelTwo;
import in.innasoft.apsrtc.models.BelowPassTypeModel;
import in.innasoft.apsrtc.models.BelowRouteCenterModel;
import in.innasoft.apsrtc.models.BelowRoutsViaModel;
import in.innasoft.apsrtc.models.BelowSchAddressModel;
import in.innasoft.apsrtc.models.BelowSchoolNameModel;
import in.innasoft.apsrtc.models.BelowToDistrictModel;
import in.innasoft.apsrtc.models.BelowVillagesModel;
import in.innasoft.apsrtc.utilities.AppUrls;
import in.innasoft.apsrtc.utilities.Config;
import in.innasoft.apsrtc.utilities.FilePath;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class BelowSSC extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Typeface typeface,typeface2;
    ImageView gotoHome,profile_pic_img;
    TextInputLayout below_name_til,below_fname_til,below_aadhar_number_til,below_phone_number_til,below_email_til,admission_number_til
            ,below_staff_id_til,below_address_til;
    TextView student_details_txt,photo_txt,welcome_headding_tv,gender_txt,isEmployee_txt,or_txt,res_address_txt,school_details_txt,route_details_txt
            ,buttom_com_tv,school_address_txt,below_dob_txt,below_ssc_dist_txt;
    EditText name_edt,fname_edt,aadhar_number_edt,phone_number_edt,email_edt,admission_number_edt,below_staff_id_edt,address_edt,
            below_ssc_to_act,below_ssc_from_act;
    RadioGroup subuser_gender_id,isemployee_rg;
    RadioButton subuser_male_radio,subuser_female_radio,isemp_yes,isemp_no;
    Button gallery_btn,take_btn,submit_btn;

    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;

    private static final String TAG = AboveSSC.class.getSimpleName();
    private Uri fileUri;
    private String selectedFilePath;

    private int mYear, mMonth, mDay;

    Spinner below_ssc_spinner1,below_ssc_spinner2,below_ssc_spinner3,below_ssc_spinner4,below_ssc_spinner5,below_ssc_spinner6,below_ssc_spinner7
            ,below_ssc_spinner8,below_ssc_spinner9,below_ssc_spinner10,below_ssc_spinner11,below_ssc_spinner12;

    ArrayAdapter<String> spinnerArrayAdapterDistricts,spinnerArrayAdapterMandals,spinnerArrayAdapterVillages;
    ArrayAdapter<String> spinnerArrayAdapterDist,spinnerArrayAdapterMnd;
    ArrayAdapter<String> spinnerArrayAdapterSchool;
    ArrayAdapter<String> spinnerArrayAdapterClass;

    ArrayAdapter<String> spinnerArrayAdapterCRouteCenter;
    ArrayAdapter<String> spinnerArrayAdapterPassType;

    ArrayAdapter<String> spinnerArrayAdapterFromRouteDistrict;
    ArrayAdapter<String> spinnerArrayAdapterToRouteDistrict;
    ArrayAdapter<String> spinnerArrayAdapterToRouteVia;

    AutoCompleteTextView fromAutoCompleteTextView,toAutoCompleteTextView;
    ArrayAdapter<String> fromAutoCompleteAdapter,toAutoCompleteAdapter;

    ArrayList<BelowDistrictsModel> districtsModels,districtsModel2;
    ArrayList<BelowMandalsModel> mandalsModels,mandalsModel2;
    ArrayList<BelowVillagesModel> villagesModels,villagesModel2;
    ArrayList<String> districtsString,mandalsString,villagesString;

    ArrayList<BelowDistrictsModelTwo> schDistrictsModels,schDistrictsModel2;
    ArrayList<BelowMandalsModelTwo> schMandalsModels,schMandalsModel2;
    ArrayList<String> schDistrictsString,schMandalsString;

    ArrayList<BelowSchoolNameModel> schoolNameModels,schoolNameModel2;
    ArrayList<String> schoolNameString;

    ArrayList<BelowClassNameModel> classNameModels,classNameModel2;
    ArrayList<String> classNameString;

    ArrayList<BelowSchAddressModel> schAddressModels,schAddressModel2;
    ArrayList<String> schAddressString;

    ArrayList<BelowPassTypeModel> passTypeModels,passTypeModel2;
    ArrayList<String> passTypeString;


    ArrayList<BelowRouteCenterModel> routeCenterModels,routeCenterModel2;
    ArrayList<String> routeCenterString;


    ArrayList<BelowFromDistrictModel> formrouteDistrictModels,formrouteDistrictModels2;
    ArrayList<String> formrouteDistrictModelsString;

    ArrayList<BelowToDistrictModel> torouteDistrictModels,torouteDistrictModels2;
    ArrayList<String> torouteDistrictModelsString;


    ArrayList<BelowRoutsViaModel> routsViaModels,routsViaModels2;
    ArrayList<String> routsViaModelsString;

    ArrayList<String> fromStopsArrayList = new ArrayList<String>();
    ArrayList<String> toStopsArrayList = new ArrayList<String>();

    Map<String, String> fromStopsID = new HashMap<String, String>();
    Map<String, String> toStopsID = new HashMap<String, String>();

    String from_stop_id,to_stop_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_below_ssc);

        below_ssc_from_act = (EditText) findViewById(R.id.below_ssc_from_act);
//        fromAutoCompleteTextView.setThreshold(1);
        below_ssc_to_act = (EditText) findViewById(R.id.below_ssc_to_act);
//        toAutoCompleteTextView.setThreshold(1);

        districtsModels = new ArrayList<BelowDistrictsModel>();
        districtsModel2 = new ArrayList<BelowDistrictsModel>();
        districtsString = new ArrayList<String>();
        districtsString.add("Select District");

        mandalsModels = new ArrayList<BelowMandalsModel>();
        mandalsModel2 = new ArrayList<BelowMandalsModel>();
        mandalsString = new ArrayList<String>();
        mandalsString.add("Select Mandal");

        villagesModels = new ArrayList<BelowVillagesModel>();
        villagesModel2 = new ArrayList<BelowVillagesModel>();
        villagesString = new ArrayList<String>();
        villagesString.add("Select Village");

        schDistrictsModels = new ArrayList<BelowDistrictsModelTwo>();
        schDistrictsModel2 = new ArrayList<BelowDistrictsModelTwo>();
        schDistrictsString = new ArrayList<String>();
        schDistrictsString.add("Select District");

        schMandalsModels = new ArrayList<BelowMandalsModelTwo>();
        schMandalsModel2 = new ArrayList<BelowMandalsModelTwo>();
        schMandalsString = new ArrayList<String>();
        schMandalsString.add("Select Mandal");


        schoolNameModels = new ArrayList<BelowSchoolNameModel>();
        schoolNameModel2 = new ArrayList<BelowSchoolNameModel>();
        schoolNameString = new ArrayList<String>();
        schoolNameString.add("Select School Name");

        classNameModels = new ArrayList<BelowClassNameModel>();
        classNameModel2 = new ArrayList<BelowClassNameModel>();
        classNameString = new ArrayList<String>();
        classNameString.add("Select Class");

        schAddressModels = new ArrayList<BelowSchAddressModel>();
        schAddressModel2 = new ArrayList<BelowSchAddressModel>();
        schAddressString = new ArrayList<String>();

        routeCenterModels = new ArrayList<BelowRouteCenterModel>();
        routeCenterModel2 = new ArrayList<BelowRouteCenterModel>();
        routeCenterString = new ArrayList<String>();
        routeCenterString.add("Select Center");

        passTypeModels = new ArrayList<BelowPassTypeModel>();
        passTypeModel2 = new ArrayList<BelowPassTypeModel>();
        passTypeString = new ArrayList<String>();
        passTypeString.add("Select Pass type");

        formrouteDistrictModels = new ArrayList<BelowFromDistrictModel>();
        formrouteDistrictModels2 = new ArrayList<BelowFromDistrictModel>();
        formrouteDistrictModelsString = new ArrayList<String>();
        formrouteDistrictModelsString.add("Select From District");


        torouteDistrictModels = new ArrayList<BelowToDistrictModel>();
        torouteDistrictModels2 = new ArrayList<BelowToDistrictModel>();
        torouteDistrictModelsString = new ArrayList<String>();
        torouteDistrictModelsString.add("Select To District");



        routsViaModels = new ArrayList<BelowRoutsViaModel>();
        routsViaModels2 = new ArrayList<BelowRoutsViaModel>();
        routsViaModelsString = new ArrayList<String>();
        routsViaModelsString.add("Select");

        typeface = Typeface.createFromAsset(getAssets(), "aquawaxlight.ttf");
        typeface2 = Typeface.createFromAsset(getAssets(), "opensansregular.ttf");

        gotoHome = (ImageView) findViewById(R.id.gotoHome);
        gotoHome.setOnClickListener(this);

        profile_pic_img = (ImageView) findViewById(R.id.profile_pic_img);

//        below_name_til = (TextInputLayout) findViewById(R.id.below_name_til);
//        below_name_til.setTypeface(typeface2);
//
//        below_fname_til = (TextInputLayout) findViewById(R.id.below_fname_til);
//        below_fname_til.setTypeface(typeface2);
//
//        below_address_til = (TextInputLayout) findViewById(R.id.below_address_til);
//        below_address_til.setTypeface(typeface2);
//
//        below_aadhar_number_til = (TextInputLayout) findViewById(R.id.below_aadhar_number_til);
//        below_aadhar_number_til.setTypeface(typeface2);
//
//        below_phone_number_til = (TextInputLayout) findViewById(R.id.below_phone_number_til);
//        below_phone_number_til.setTypeface(typeface2);
//
//        below_email_til = (TextInputLayout) findViewById(R.id.below_email_til);
//        below_email_til.setTypeface(typeface2);
//
//        admission_number_til = (TextInputLayout) findViewById(R.id.admission_number_til);
//        admission_number_til.setTypeface(typeface2);

//        below_staff_id_til = (TextInputLayout) findViewById(R.id.below_staff_id_til);
//        below_staff_id_til.setTypeface(typeface2);

//        welcome_headding_tv = (TextView) findViewById(R.id.welcome_headding_tv);
//        welcome_headding_tv.setTypeface(typeface);

        student_details_txt = (TextView) findViewById(R.id.student_details_txt);
        student_details_txt.setTypeface(typeface2);

        gender_txt = (TextView) findViewById(R.id.gender_txt);
        gender_txt.setTypeface(typeface2);

        isEmployee_txt = (TextView) findViewById(R.id.isEmployee_txt);
        isEmployee_txt.setTypeface(typeface2);

        photo_txt = (TextView) findViewById(R.id.photo_txt);
        photo_txt.setTypeface(typeface2);

        or_txt = (TextView) findViewById(R.id.or_txt);
        or_txt.setTypeface(typeface2);

        res_address_txt = (TextView) findViewById(R.id.res_address_txt);
        res_address_txt.setTypeface(typeface2);

        school_details_txt = (TextView) findViewById(R.id.school_details_txt);
        school_details_txt.setTypeface(typeface2);

        route_details_txt = (TextView) findViewById(R.id.route_details_txt);
        route_details_txt.setTypeface(typeface2);

        below_ssc_dist_txt = (TextView) findViewById(R.id.below_ssc_dist_txt);
        below_ssc_dist_txt.setTypeface(typeface2);

        buttom_com_tv = (TextView) findViewById(R.id.buttom_com_tv);
        buttom_com_tv.setTypeface(typeface);

        name_edt = (EditText) findViewById(R.id.name_edt);
        name_edt.setTypeface(typeface2);

        fname_edt = (EditText) findViewById(R.id.fname_edt);
        fname_edt.setTypeface(typeface2);

        below_dob_txt = (TextView) findViewById(R.id.below_dob_txt);
        below_dob_txt.setTypeface(typeface2);
        below_dob_txt.setOnClickListener(this);

        aadhar_number_edt = (EditText) findViewById(R.id.aadhar_number_edt);
        aadhar_number_edt.setTypeface(typeface2);

        address_edt = (EditText) findViewById(R.id.address_edt);
        address_edt.setTypeface(typeface2);

        phone_number_edt = (EditText) findViewById(R.id.phone_number_edt);
        phone_number_edt.setTypeface(typeface2);

        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setTypeface(typeface2);

        admission_number_edt = (EditText) findViewById(R.id.admission_number_edt);
        admission_number_edt.setTypeface(typeface2);

        below_staff_id_edt = (EditText) findViewById(R.id.below_staff_id_edt);
        below_staff_id_edt.setTypeface(typeface2);

        school_address_txt = (TextView) findViewById(R.id.school_address_txt);
        school_address_txt.setTypeface(typeface2);

        subuser_gender_id = (RadioGroup) findViewById(R.id.subuser_gender_id);
        isemployee_rg = (RadioGroup) findViewById(R.id.isemployee_rg);

        subuser_male_radio = (RadioButton) findViewById(R.id.subuser_male_radio);
        subuser_male_radio.setTypeface(typeface2);

        subuser_female_radio = (RadioButton) findViewById(R.id.subuser_female_radio);
        subuser_female_radio.setTypeface(typeface2);

        isemp_yes = (RadioButton) findViewById(R.id.isemp_yes);
        isemp_yes.setTypeface(typeface2);

        isemp_no = (RadioButton) findViewById(R.id.isemp_no);
        isemp_no.setTypeface(typeface2);

        gallery_btn = (Button) findViewById(R.id.gallery_btn);
        gallery_btn.setTypeface(typeface2);
        gallery_btn.setOnClickListener(this);

        take_btn = (Button) findViewById(R.id.take_btn);
        take_btn.setTypeface(typeface2);
        take_btn.setOnClickListener(this);

        submit_btn = (Button) findViewById(R.id.submit_btn);
        submit_btn.setTypeface(typeface2);
        submit_btn.setOnClickListener(this);


        isemployee_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                if(rb.getText().equals("No"))
                {

                    below_staff_id_edt.setVisibility(View.GONE);
                }else {

                    below_staff_id_edt.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        below_ssc_spinner1 = (Spinner) findViewById(R.id.below_ssc_spinner1);
        below_ssc_spinner2 = (Spinner) findViewById(R.id.below_ssc_spinner2);
        below_ssc_spinner3 = (Spinner) findViewById(R.id.below_ssc_spinner3);
        below_ssc_spinner4 = (Spinner) findViewById(R.id.below_ssc_spinner4);
        below_ssc_spinner5 = (Spinner) findViewById(R.id.below_ssc_spinner5);
        below_ssc_spinner6 = (Spinner) findViewById(R.id.below_ssc_spinner6);
        below_ssc_spinner7 = (Spinner) findViewById(R.id.below_ssc_spinner7);
        below_ssc_spinner8 = (Spinner) findViewById(R.id.below_ssc_spinner8);
        below_ssc_spinner9 = (Spinner) findViewById(R.id.below_ssc_spinner9);
        below_ssc_spinner10 = (Spinner) findViewById(R.id.below_ssc_spinner10);
        below_ssc_spinner11 = (Spinner) findViewById(R.id.below_ssc_spinner11);
        below_ssc_spinner12 = (Spinner) findViewById(R.id.below_ssc_spinner12);


        spinnerArrayAdapterDistricts = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, districtsString);
        spinnerArrayAdapterDistricts.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner1.setAdapter(spinnerArrayAdapterMandals);



        spinnerArrayAdapterMandals = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, mandalsString);
        spinnerArrayAdapterMandals.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner2.setAdapter(spinnerArrayAdapterDistricts);


        spinnerArrayAdapterVillages = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, villagesString);
        spinnerArrayAdapterVillages.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner3.setAdapter(spinnerArrayAdapterVillages);

        spinnerArrayAdapterDist = new ArrayAdapter<String>(this, R.layout.spinner_item_all_caps,schDistrictsString);
        spinnerArrayAdapterDist.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner4.setAdapter(spinnerArrayAdapterDist);

        spinnerArrayAdapterMnd = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, schMandalsString);
        spinnerArrayAdapterMnd.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner5.setAdapter(spinnerArrayAdapterMnd);

        spinnerArrayAdapterSchool = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, schoolNameString);
        spinnerArrayAdapterSchool.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner6.setAdapter(spinnerArrayAdapterSchool);

        spinnerArrayAdapterClass = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, classNameString);
        spinnerArrayAdapterClass.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner6.setAdapter(spinnerArrayAdapterClass);


        spinnerArrayAdapterCRouteCenter = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, routeCenterString);
        spinnerArrayAdapterCRouteCenter.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner8.setAdapter(spinnerArrayAdapterCRouteCenter);



        spinnerArrayAdapterPassType = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, passTypeString);
        spinnerArrayAdapterPassType.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner9.setAdapter(spinnerArrayAdapterPassType);


        spinnerArrayAdapterFromRouteDistrict = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, formrouteDistrictModelsString);
        spinnerArrayAdapterFromRouteDistrict.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner10.setAdapter(spinnerArrayAdapterFromRouteDistrict);



        spinnerArrayAdapterToRouteDistrict = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, torouteDistrictModelsString);
        spinnerArrayAdapterToRouteDistrict.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner11.setAdapter(spinnerArrayAdapterToRouteDistrict);


        spinnerArrayAdapterToRouteVia = new ArrayAdapter<String>(
                this, R.layout.spinner_item_all_caps, routsViaModelsString);
        spinnerArrayAdapterToRouteVia.setDropDownViewResource(R.layout.spinner_item_all_caps);
        below_ssc_spinner12.setAdapter(spinnerArrayAdapterToRouteVia);


        final StringRequest stringRequest2 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            formrouteDistrictModelsString.clear();
                            formrouteDistrictModels.clear();
                            formrouteDistrictModels2.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            formrouteDistrictModelsString.add("--Select Form District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowFromDistrictModel dm = new BelowFromDistrictModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                formrouteDistrictModels.add(dm);
                                formrouteDistrictModels2.add(dm);
                                formrouteDistrictModelsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            below_ssc_spinner10.setAdapter(spinnerArrayAdapterFromRouteDistrict);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue2 = Volley.newRequestQueue(getApplicationContext());
        requestQueue2.add(stringRequest2);



        final StringRequest stringRequest3 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            torouteDistrictModelsString.clear();
                            torouteDistrictModels.clear();
                            torouteDistrictModels2.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                           /* if(!statesString.contains("Select State") && !statesModel2.contains("Select State") && !statesModel2.contains("Select State"))
                            {*/
                            torouteDistrictModelsString.add("--Select To District--");
                            //}
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowToDistrictModel dm = new BelowToDistrictModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                torouteDistrictModels.add(dm);
                                torouteDistrictModels2.add(dm);
                                torouteDistrictModelsString.add(jsonObject1.getString("district_name"));
                            }
                            //newSatesString.add(statesString);
                            // selecteState.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, statesString));
                            below_ssc_spinner11.setAdapter(spinnerArrayAdapterToRouteDistrict);
                            // above_ssc_spinner6.setAdapter(spinnerArrayAdapterDistricts);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue3 = Volley.newRequestQueue(getApplicationContext());
        requestQueue3.add(stringRequest3);




        final StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            districtsModels.clear();
                            districtsModel2.clear();
                            districtsString.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            districtsString.add("--Select District--");

                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowDistrictsModel dm = new BelowDistrictsModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                districtsModels.add(dm);
                                districtsModel2.add(dm);
                                districtsString.add(jsonObject1.getString("district_name"));
                            }

                            below_ssc_spinner1.setAdapter(spinnerArrayAdapterDistricts);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);


        below_ssc_spinner1.setOnItemSelectedListener(this);
        below_ssc_spinner2.setOnItemSelectedListener(this);
        below_ssc_spinner3.setOnItemSelectedListener(this);


        final StringRequest stringRequest1 = new StringRequest(Request.Method.GET, AppUrls.BASE_USRL+AppUrls.GETTING_DISTRICTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "SUCCESS "+response);

                            schDistrictsModels.clear();
                            schDistrictsModel2.clear();
                            schDistrictsString.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            schDistrictsString.add("--Select District--");

                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowDistrictsModelTwo dm = new BelowDistrictsModelTwo();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("district");
                                dm.setId(jsonObject1.getString("id"));
                                dm.setCountry_id(jsonObject1.getString("country_id"));
                                dm.setState_id(jsonObject1.getString("state_id"));
                                dm.setDistrict_name(jsonObject1.getString("district_name"));
                                dm.setStatus(jsonObject1.getString("status"));
                                dm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                dm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                schDistrictsModels.add(dm);
                                schDistrictsModel2.add(dm);
                                schDistrictsString.add(jsonObject1.getString("district_name"));
                            }

                            below_ssc_spinner4.setAdapter(spinnerArrayAdapterDist);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        RequestQueue requestQueue1 = Volley.newRequestQueue(getApplicationContext());
        requestQueue1.add(stringRequest1);

        below_ssc_spinner4.setOnItemSelectedListener(this);
        below_ssc_spinner5.setOnItemSelectedListener(this);
        below_ssc_spinner6.setOnItemSelectedListener(this);
        below_ssc_spinner7.setOnItemSelectedListener(this);
        below_ssc_spinner8.setOnItemSelectedListener(this);
        below_ssc_spinner9.setOnItemSelectedListener(this);
        below_ssc_spinner10.setOnItemSelectedListener(this);
        below_ssc_spinner11.setOnItemSelectedListener(this);
        below_ssc_spinner12.setOnItemSelectedListener(this);

//
//        fromAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                from_stop_id = fromStopsID.get(fromAutoCompleteAdapter.getItem(position).toString());
//
//            }
//        });
//
//
//        toAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                to_stop_id = toStopsID.get(toAutoCompleteAdapter.getItem(position).toString());
//                findRoutsVia(from_stop_id, to_stop_id);
//
//            }
//        });

    }

    private void findRoutsVia(final String from_stop_id, final String to_stop_id) {

        routsViaModels2.clear();
        routsViaModels.clear();
        routsViaModelsString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_ROUTE_VIA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("GETTINGROUTVIA ", "GETTINGROUTVIA SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            routsViaModelsString.add("--Select--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowRoutsViaModel rvm = new BelowRoutsViaModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("via");
                                rvm.setId(jsonObject1.getString("id"));
                                rvm.setVia(jsonObject1.getString("via"));
                                rvm.setKilometers(jsonObject1.getString("kilometers"));
                                rvm.setPrice(jsonObject1.getString("price"));
                                rvm.setId_charges(jsonObject1.getString("id_charges"));

                                routsViaModels.add(rvm);
                                routsViaModels2.add(rvm);
                                routsViaModelsString.add(jsonObject1.getString("via"));
                            }

                            below_ssc_spinner12.setAdapter(spinnerArrayAdapterToRouteVia);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_village_id", from_stop_id);
                params.put("to_village_id", to_stop_id);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v)
    {
        if(v == gotoHome)
        {
            Intent intent = new Intent(getApplicationContext(), HomePage.class);
            startActivity(intent);
        }
        if(v == gallery_btn)
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_REQUEST_IMAGE);
        }
        if(v == take_btn)
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            File file=getOutputMediaFile(1);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
        }
        if (v == below_dob_txt){
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null,strDay = null;

                    int month = monthOfYear+1;
                    if(month < 10){

                        strMonth = "0" + month;
                    }
                    else {
                        strMonth = month+"";
                    }
                    if(dayOfMonth < 10){

                        strDay  = "0" + dayOfMonth ;
                    }
                    else {
                        strDay = dayOfMonth+"";
                    }
                    below_dob_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));

                }

            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == submit_btn){
            validate();
        }

    }

    private boolean validate()
    {

        boolean result = true;

        String number_validation = "^[0-9]+";

        String name = name_edt.getText().toString();
        if ((name == null || name.length() < 3) && name != "^[a-zA-Z\\\\s]+") {

            below_name_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            below_name_til.setErrorEnabled(false);




        String fname = fname_edt.getText().toString();
        if ((fname == null || fname.length() < 3) && fname != "^[a-zA-Z\\\\s]+") {

            below_fname_til.setError(getString(R.string.invalid_name));
            result = false;
        }
        else
            below_fname_til.setErrorEnabled(false);





        String aadharNumber = aadhar_number_edt.getText().toString();
        if (!aadharNumber.matches(number_validation) || aadharNumber.length() != 12) {
            below_aadhar_number_til.setError(getString(R.string.invalid_aadhar_number));
            result = false;
        }
        else {
            below_aadhar_number_til.setErrorEnabled(false);
        }



        String phoneNumber = phone_number_edt.getText().toString();
        if (!phoneNumber.matches(number_validation) || phoneNumber.length() != 10) {
            below_phone_number_til.setError(getString(R.string.invalid_phone_number));
            result = false;
        }
        else {
            below_phone_number_til.setErrorEnabled(false);
        }




        String admissionNumber = admission_number_edt.getText().toString();
        if (!admissionNumber.matches(number_validation) || admissionNumber.length() != 15) {
            admission_number_til.setError(getString(R.string.invalid_admision_number));
            result = false;
        }
        else {
            admission_number_til.setErrorEnabled(false);
        }


        return result;
    }

    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }


    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST_IMAGE)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {

        selectedFilePath = FilePath.getPath(this, fileUri);

        Log.i(TAG, "Selected File Path:" + selectedFilePath);

        Thread thread=new Thread(new Runnable(){
            public void run(){

            }
        });
        thread.start();

        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[24 * 1024];
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Bitmap bmp1 = BitmapFactory.decodeFile(selectedFilePath, options);
            Bitmap b1 = ThumbnailUtils.extractThumbnail(bmp1, 220, 220);
            profile_pic_img.setImageBitmap(b1);
            if (bmp1 != null) {
                bmp1.recycle();
            }

        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.i(TAG, "Selected File Path:" + selectedFilePath);


        Thread thread=new Thread(new Runnable(){
            public void run(){


            }
        });
        thread.start();

        String name = data.getData().getPath();
        Log.d("IMAGEPIC", name);

        profile_pic_img.setImageBitmap(bm);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if(spinner.getId() == R.id.below_ssc_spinner1)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = districtsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < districtsModel2.size(); i++)
                {
                    findids.add(districtsModel2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                int selectionPosition= spinnerArrayAdapterFromRouteDistrict.getPosition(item);
                below_ssc_spinner10.setSelection(selectionPosition);

                findMandals(itemValue);
                findRouteCenter(itemValue);
            }

            else {

                below_ssc_spinner10.setSelection(0);
            }
        }

        if(spinner.getId() == R.id.below_ssc_spinner2)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = mandalsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < mandalsModel2.size(); i++)
                {
                    findids.add(mandalsModel2.get(i).getMandal_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                findVillages(itemValue);
            }

            else {

            }
        }


        if(spinner.getId() == R.id.below_ssc_spinner4)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = schDistrictsModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < schDistrictsModel2.size(); i++)
                {
                    findids.add(schDistrictsModel2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                if(below_ssc_spinner1.getSelectedItemPosition() > 0) {
                    int mPossition = below_ssc_spinner1.getSelectedItemPosition();
                    String valueone = districtsModel2.get(mPossition - 1).getId();
                    String finalString = valueone + "," + itemValue;
                    findRouteCenter(finalString);
                }

                int selectionPosition= spinnerArrayAdapterToRouteDistrict.getPosition(item);
                below_ssc_spinner11.setSelection(selectionPosition);
                findSchMandals(itemValue);

            }

            else {
                below_ssc_spinner11.setSelection(0);
            }
        }

        if(spinner.getId() == R.id.below_ssc_spinner5) {
            if (position != 0) {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = schMandalsModel2.get(position - 1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i = 0; i < schMandalsModel2.size(); i++) {
                    findids.add(schMandalsModel2.get(i).getMandal_name());
                }
                Log.d("2KTEC---", item + "---" + itemValue);

                findSchoolName(itemValue);
            } else {

            }
        }

        if(spinner.getId() == R.id.below_ssc_spinner6)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = schoolNameModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < schoolNameModel2.size(); i++)
                {
                    findids.add(schoolNameModel2.get(i).getInstitution_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                findClass(itemValue);
                findSchAddress(itemValue);
            }

            else {

            }
        }


        if(spinner.getId() == R.id.below_ssc_spinner8)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = routeCenterModel2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < routeCenterModel2.size(); i++)
                {
                    findids.add(routeCenterModel2.get(i).getCenter_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);

                //Toast.makeText(getApplicationContext(), item+"---"+itemValue, Toast.LENGTH_SHORT).show();

                //findMandals(itemValue);
                findPassType(itemValue);
            }

            else {

                //selectDistric.setSelection(0);
                // selectDistric.setPrompt("Select District");
                // districtString.clear();
                // districtsModels2.clear();
                //districtsModels.clear();
            }
        }


        if(spinner.getId() == R.id.below_ssc_spinner10)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = formrouteDistrictModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < formrouteDistrictModels2.size(); i++)
                {
                    findids.add(formrouteDistrictModels2.get(i).getDistrict_name());
                }
                Log.d("2KTECK", item+"---"+itemValue);
                findFromStops(itemValue);

            }

            else {

            }
        }
        if(spinner.getId() == R.id.below_ssc_spinner11)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = torouteDistrictModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < torouteDistrictModels2.size(); i++)
                {
                    findids.add(torouteDistrictModels2.get(i).getDistrict_name());
                }
                Log.d("2KTEC", item+"---"+itemValue);
                findToStops(itemValue);
            }

            else {

            }
        }


        if(spinner.getId() == R.id.below_ssc_spinner12)
        {
            if(position != 0)
            {
                String item = parent.getItemAtPosition(position).toString();
                String itemValue = routsViaModels2.get(position-1).getId();
                ArrayList<String> findids = new ArrayList<String>();
                for (int i =0 ; i < routsViaModels2.size(); i++)
                {
                    findids.add(routsViaModels2.get(i).getVia());
                }
                long totalAmount = Long.parseLong(routsViaModels2.get(position-1).getPrice())+30;
                below_ssc_dist_txt.setText("Distance(in KMs):"+routsViaModels2.get(position-1).getKilometers()+" Pass Amount(₹):"+routsViaModels2.get(position-1).getPrice()+" Id Charges(₹): 30 /-"+" Total Amount(₹): "+totalAmount+" /-");

            }

            else {

            }
        }

    }

    private void findToStops(final String itemValue) {

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_TO_STOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");

                                toStopsArrayList.add((jsonObject1.getString("village_name")).toUpperCase());
                                toStopsID.put((jsonObject1.getString("village_name")).toUpperCase(),jsonObject1.getString("id"));

                            }
                            String[] myArray = new String[toStopsArrayList.size()];
                            toStopsArrayList.toArray(myArray);
                            toAutoCompleteAdapter = new ArrayAdapter<String>
                                    (getApplicationContext(),R.layout.spinner_item_all_caps,myArray);

                            toAutoCompleteTextView.setAdapter(toAutoCompleteAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("to_district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findFromStops(final String itemValue) {
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_FROM_STOPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("FROMSTOPSFIND ", "FROMSTOPSFIND SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");

                                fromStopsArrayList.add((jsonObject1.getString("village_name")).toUpperCase());
                                fromStopsID.put((jsonObject1.getString("village_name")).toUpperCase(),jsonObject1.getString("id"));

                            }
                            String[] myArray = new String[fromStopsArrayList.size()];
                            fromStopsArrayList.toArray(myArray);
                            fromAutoCompleteAdapter = new ArrayAdapter<String>
                                    (getApplicationContext(),R.layout.spinner_item_all_caps,myArray);



                            Log.d("DISPLAYDATA", ""+fromStopsID.toString());
                            fromAutoCompleteTextView.setAdapter(fromAutoCompleteAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findRouteCenter(final String itemValue) {
        routeCenterModel2.clear();
        routeCenterModels.clear();
        routeCenterString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_CENTERS_IN_ROUTE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "Center SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            routeCenterString.add("--Select Center--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowRouteCenterModel rm = new BelowRouteCenterModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("center");
                                rm.setId(jsonObject1.getString("id"));
                                rm.setCountry_id(jsonObject1.getString("country_id"));
                                rm.setState_id(jsonObject1.getString("state_id"));
                                rm.setDistrict_id(jsonObject1.getString("district_id"));
                                rm.setCenter_name(jsonObject1.getString("center_name"));
                                rm.setStatus(jsonObject1.getString("status"));
                                rm.setMandal_idl(jsonObject1.getString("mandal_id"));
                                rm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                rm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                routeCenterModels.add(rm);
                                routeCenterModel2.add(rm);
                                routeCenterString.add(jsonObject1.getString("center_name"));
                            }

                            below_ssc_spinner8.setAdapter(spinnerArrayAdapterCRouteCenter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findPassType(final String itemValue) {
        passTypeModel2.clear();
        passTypeModels.clear();
        passTypeString.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_PASS_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "PassType SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            passTypeString.add("--Select Pass type--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowPassTypeModel pm = new BelowPassTypeModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("pass_type");
                                pm.setId(jsonObject1.getString("id"));
                                pm.setCountry_id(jsonObject1.getString("country_id"));
                                pm.setState_id(jsonObject1.getString("state_id"));
                                pm.setDistrict_id(jsonObject1.getString("district_id"));
                                pm.setMandal_id(jsonObject1.getString("mandal_id"));
                                pm.setCenter_id(jsonObject1.getString("center_id"));
                                pm.setPass_type(jsonObject1.getString("pass_type"));
                                pm.setStatus(jsonObject1.getString("status"));

                                pm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                pm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                passTypeModels.add(pm);
                                passTypeModel2.add(pm);
                                passTypeString.add(jsonObject1.getString("pass_type"));
                            }

                            below_ssc_spinner9.setAdapter(spinnerArrayAdapterPassType);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("center_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findSchAddress(final String itemValue) {

        final StringRequest stringRequest2 = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_INSTADDRESS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("ADDRESS", "SUCCESS" + response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution_address");
                                jsonObject1.getString("id");
                                jsonObject1.getString("country_id");
                                jsonObject1.getString("state_id");
                                jsonObject1.getString("district_id");
                                jsonObject1.getString("mandal_id");
                                jsonObject1.getString("institution_id");

                                school_address_txt.setText(jsonObject1.getString("institution_address"));

                                jsonObject1.getString("status");
                                jsonObject1.getString("created_date_time");
                                jsonObject1.getString("updated_date_time");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("institution_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue2 = Volley.newRequestQueue(getApplicationContext());
        requestQueue2.add(stringRequest2);


    }

    private void findClass(final String itemValue) {
        classNameString.clear();
        classNameModel2.clear();
        classNameModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_COURSENAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("Class", "SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            classNameString.add("--Select--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowClassNameModel cnm = new BelowClassNameModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution_course");
                                cnm.setId(jsonObject1.getString("id"));
                                cnm.setCountry_id(jsonObject1.getString("country_id"));
                                cnm.setState_id(jsonObject1.getString("state_id"));
                                cnm.setDistrict_id(jsonObject1.getString("district_id"));
                                cnm.setMandal_id(jsonObject1.getString("mandal_id"));
                                cnm.setInstitution_id(jsonObject1.getString("institution_id"));
                                cnm.setInstitution_cource_name(jsonObject1.getString("institution_cource_name"));
                                cnm.setStatus(jsonObject1.getString("status"));
                                cnm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                cnm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                classNameModels.add(cnm);
                                classNameModel2.add(cnm);
                                classNameString.add(jsonObject1.getString("institution_cource_name"));
                            }

                            below_ssc_spinner7.setAdapter(spinnerArrayAdapterClass);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("institution_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findSchoolName(final String itemValue) {
        schoolNameString.clear();
        schoolNameModel2.clear();
        schoolNameModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_INSTITUTIONNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("SCHOOL", "SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            schoolNameString.add("--Select School Name--");

                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowSchoolNameModel inm = new BelowSchoolNameModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("institution");
                                inm.setId(jsonObject1.getString("id"));
                                inm.setInstitution_type(jsonObject1.getString("institution_type"));
                                inm.setCountry_id(jsonObject1.getString("country_id"));
                                inm.setState_id(jsonObject1.getString("state_id"));
                                inm.setDistrict_id(jsonObject1.getString("district_id"));
                                inm.setMandal_id(jsonObject1.getString("mandal_id"));
                                inm.setInstitution_name(jsonObject1.getString("institution_name"));
                                inm.setStatus(jsonObject1.getString("status"));
                                inm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                inm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                schoolNameModels.add(inm);
                                schoolNameModel2.add(inm);
                                schoolNameString.add(jsonObject1.getString("institution_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            below_ssc_spinner6.setAdapter(spinnerArrayAdapterSchool);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mandal_id", itemValue);
                params.put("institution_type","upto_ssc");

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void findSchMandals(final String itemValue) {
        schMandalsString.clear();
        schMandalsModel2.clear();
        schMandalsModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_MANDALS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "DISTRICS SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            schMandalsString.add("--Select Mandal--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowMandalsModelTwo mm = new BelowMandalsModelTwo();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("mandal");
                                mm.setId(jsonObject1.getString("id"));
                                mm.setCountry_id(jsonObject1.getString("country_id"));
                                mm.setState_id(jsonObject1.getString("state_id"));
                                mm.setDistrict_id(jsonObject1.getString("district_id"));
                                mm.setMandal_name(jsonObject1.getString("mandal_name"));
                                mm.setStatus(jsonObject1.getString("status"));
                                mm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                mm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                schMandalsModels.add(mm);
                                schMandalsModel2.add(mm);
                                schMandalsString.add(jsonObject1.getString("mandal_name"));
                            }



                            //newSatesString.add(statesString);


                            //selectDistric.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtString));

                            below_ssc_spinner5.setAdapter(spinnerArrayAdapterMnd);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findVillages(final String itemValue) {
        villagesString.clear();
        villagesModel2.clear();
        villagesModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_VILLAGES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "VILLAGES SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            villagesString.add("--Select Village--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowVillagesModel vm = new BelowVillagesModel();

                                Log.d("CHINTHALLURU", "CHECK");

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("village");
                                vm.setId(jsonObject1.getString("id"));
                                vm.setCountry_id(jsonObject1.getString("country_id"));
                                vm.setState_id(jsonObject1.getString("state_id"));
                                vm.setDistrict_id(jsonObject1.getString("district_id"));
                                vm.setMandal_id(jsonObject1.getString("mandal_id"));
                                vm.setVillage_name(jsonObject1.getString("village_name"));
                                vm.setStatus(jsonObject1.getString("status"));
                                vm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                vm.setUpdate_date_time(jsonObject1.getString("update_date_time"));

                                villagesModels.add(vm);
                                villagesModel2.add(vm);
                                villagesString.add(jsonObject1.getString("village_name"));
                            }

                            below_ssc_spinner3.setAdapter(spinnerArrayAdapterVillages);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mandal_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void findMandals(final String itemValue) {
        mandalsString.clear();
        mandalsModel2.clear();
        mandalsModels.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_USRL+AppUrls.GETTING_MANDALS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("REGISTRATION ", "DISTRICS SUCCESS "+response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            mandalsString.add("--Select Mandal--");
                            for (int i =0 ; i < jsonArray.length(); i++)
                            {
                                BelowMandalsModel mm = new BelowMandalsModel();

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("mandal");
                                mm.setId(jsonObject1.getString("id"));
                                mm.setCountry_id(jsonObject1.getString("country_id"));
                                mm.setState_id(jsonObject1.getString("state_id"));
                                mm.setDistrict_id(jsonObject1.getString("district_id"));
                                mm.setMandal_name(jsonObject1.getString("mandal_name"));
                                mm.setStatus(jsonObject1.getString("status"));
                                mm.setCreated_date_time(jsonObject1.getString("created_date_time"));
                                mm.setUpdated_date_time(jsonObject1.getString("updated_date_time"));

                                mandalsModels.add(mm);
                                mandalsModel2.add(mm);
                                mandalsString.add(jsonObject1.getString("mandal_name"));
                            }

                            below_ssc_spinner2.setAdapter(spinnerArrayAdapterMandals);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("district_id", itemValue);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
