package in.innasoft.apsrtc.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.innasoft.apsrtc.R;

public class ValidateSuccessActivity extends AppCompatActivity {
TextView txtCurrentDateTime,txtAmount;
    Animation zoomIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_success);

        String result = getIntent().getStringExtra("code");

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        txtCurrentDateTime =findViewById(R.id.txtCurrentDateTime);
        txtCurrentDateTime.setText(formattedDate);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(5000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
       // anim.setRepeatCount(Animation.INFINITE);

       // zoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        txtCurrentDateTime.startAnimation(anim);

        txtAmount = findViewById(R.id.price1);
        txtAmount.setText("\u20B9"+"460/-");

    }
}
