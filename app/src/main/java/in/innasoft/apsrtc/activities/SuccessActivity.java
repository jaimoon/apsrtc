package in.innasoft.apsrtc.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import in.innasoft.apsrtc.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {
    private QRGEncoder qrgEncoder;
    private Bitmap bitmapResult;
    private ImageView imageView;
    Button btnSave,gotoHome;
    String uniqueId,name,phone,email;
    private View mView;
    TextView name_tv,id_tv,mobile_tv,email_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);


        btnSave = (Button) findViewById(R.id.save);

        Bundle bundle = getIntent().getExtras();
        uniqueId = bundle.getString("uniqueid");
        name = bundle.getString("name");
        phone = bundle.getString("phone");
        email = bundle.getString("email");



        mView = findViewById(R.id.f_view);
        imageView = (ImageView) mView.findViewById(R.id.QR_Image);
        name_tv = (TextView) mView.findViewById(R.id.name_of_student);
        name_tv.setText(name);
        id_tv = (TextView) mView.findViewById(R.id.id_of_student);
        id_tv.setText(uniqueId);
        mobile_tv = (TextView) mView.findViewById(R.id.phone_of_student);
        mobile_tv.setText(phone);
        email_tv = (TextView) mView.findViewById(R.id.email_of_student);
        email_tv.setText(email);

        gotoHome = (Button) findViewById(R.id.gotoHome);
        gotoHome.setOnClickListener(this);


        mView.setDrawingCacheEnabled(true);
        mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        mView.layout(0, 0, mView.getMeasuredWidth(), mView.getMeasuredHeight());
        mView.buildDrawingCache(true);


        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        qrgEncoder = new QRGEncoder(uniqueId, null, QRGContents.Type.TEXT, smallerDimension);
        try {
            bitmapResult = qrgEncoder.encodeAsBitmap();
            imageView.setImageBitmap(bitmapResult);
            btnSave.setVisibility(View.VISIBLE);
        } catch (WriterException e) {

        }


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* boolean save;
                String result;
                try {
                    String savePath = Environment.getExternalStorageDirectory() + "/QRCode/";

                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    save = QRGSaver.save(savePath, uniqueId+"_"+name, bitmapResult, QRGContents.ImageType.IMAGE_JPEG);
                    result = save ? "Image Saved" : "Image Not Saved";
                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/



                Bitmap b = Bitmap.createBitmap(mView.getDrawingCache());
                mView.setDrawingCacheEnabled(false);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + timestamp+".jpg");
                try {
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (Exception e) {
                }

                Log.e("Folder",""+f);
                btnSave.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Successfully Saved...!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View view) {
        if(view == gotoHome)
        {
            Intent intent = new Intent(getApplicationContext(), HomePage.class);
            startActivity(intent);
            finish();
        }
    }
}
