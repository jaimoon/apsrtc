package in.innasoft.apsrtc.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.utilities.AppUrls;
import in.innasoft.apsrtc.utilities.Config;
import in.innasoft.apsrtc.utilities.FilePath;
import in.innasoft.apsrtc.utilities.ImagePermissions;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class DemoRegistration extends AppCompatActivity implements View.OnClickListener {
    EditText name_et,email_et,phone_et;
    Button submit_bt;

    ImageView imageButton;
    private Uri mCropImageUri = null;
    HttpEntity resEntity;
    private String userChoosenTask;
    private Uri fileUri;
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = DemoRegistration.class.getSimpleName();
    private String selectedFilePath;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_registration);


        progressDialog = new ProgressDialog(DemoRegistration.this, R.style.MyProDialogTheme);
        //dialog.setProgressStyle(BIND_ADJUST_WITH_ACTIVITY);

        progressDialog.setMessage("Registering......");
        progressDialog.setCancelable(false);


        name_et = (EditText) findViewById(R.id.name_et);
        email_et = (EditText) findViewById(R.id.email_et);
        phone_et = (EditText) findViewById(R.id.phone_et);

        submit_bt = (Button) findViewById(R.id.submit_bt);


        submit_bt.setOnClickListener(this);


        imageButton = (ImageView) findViewById(R.id.quick_start_cropped_image);
        imageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view == imageButton)
        {
            // onSelectImageClick(view);


            final String[] country = {"Select from Gallery", "Take a Photo"};
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select One")
                    .setItems(country, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            String selectedCountry = country[i];

                            boolean result = ImagePermissions.checkPermission(DemoRegistration.this);

                            if (selectedCountry.equals("Take a Photo")) {
                                userChoosenTask = "Take a Photo";
                                if (result)
                                    cameraIntent();

                            } else if (selectedCountry.equals("Select from Gallery")) {
                                userChoosenTask = "Select from Gallery";
                                if (result)
                                    galleryIntent();

                            }

                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        if (view == submit_bt)
        {

            if(selectedFilePath != null) {

                if(name_et.getText().toString().trim().length() > 0 && email_et.getText().toString().trim().length() > 0 && phone_et.getText().toString().trim().length() > 0) {
                    //  final String checkImageURL = mCropImageUri.toString();
                    final String name = name_et.getText().toString();
                    final String email = email_et.getText().toString();
                    final String phone = phone_et.getText().toString();

                    Intent intent = new Intent(DemoRegistration.this, SuccessActivity.class);
                    intent.putExtra("uniqueid","APSRTC123456");
                    intent.putExtra("name",name);
                    intent.putExtra("phone",phone);
                    intent.putExtra("email",email);
                    startActivity(intent);
                    finish();


                   /* if ((!name.equals("") && name.length() >= 3)) {

                        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
                        if (email.matches(EMAIL_REGEX)) {
                            if ((!phone.equals("")) && phone.length() == 10) {
                                progressDialog.show();

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        try {
                                            String applicationForm = applicationFormMY(selectedFilePath,name,email,phone);

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progressDialog.dismiss();
                                                }
                                            });


                                            try {
                                                JSONObject jsonObject = new JSONObject(applicationForm);

                                                Log.d("RESPONCESTATUS", applicationForm);

                                                String name = jsonObject.getString("name");
                                                String phone = jsonObject.getString("phone");
                                                String email = jsonObject.getString("email");
                                                String uniqueId = jsonObject.getString("uniqueid");

                                                Intent intent = new Intent(DemoRegistration.this, SuccessActivity.class);
                                                intent.putExtra("uniqueid",uniqueId);
                                                intent.putExtra("name",name);
                                                intent.putExtra("phone",phone);
                                                intent.putExtra("email",email);
                                                startActivity(intent);
                                                finish();



                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }).start();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Invalid Phone Number....!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Invalid Email Id....!", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Invalid Name....!", Toast.LENGTH_SHORT).show();
                    }*/




                }else {
                    Toast.makeText(getApplicationContext(), "Please provide all details...!", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Please select/take image", Toast.LENGTH_SHORT).show();
            }

        }
    }





    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),GALLERY_REQUEST_IMAGE);
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file=getOutputMediaFile(1);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
    }

    private Uri getOutputMediaFileUri(int mediaTypeVideo) {

        return Uri.fromFile(getOutputMediaFile(mediaTypeVideo));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {






        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST_IMAGE)
                onCaptureImageResult(data);
        }


    }

    private void onCaptureImageResult(Intent data) {

        // selectedFilePath = FilePath.getPath(this, fileUri);
        selectedFilePath = compressImage(FilePath.getPath(this, fileUri));

        Log.i(TAG, "Selected File Path:" + selectedFilePath);


        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[24 * 1024];
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Bitmap bmp1 = BitmapFactory.decodeFile(selectedFilePath, options);
            Bitmap b1 = ThumbnailUtils.extractThumbnail(bmp1, 220, 220);
            imageButton.setImageBitmap(b1);
            if (bmp1 != null) {
                bmp1.recycle();
            }

        }


    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {               imgRatio = maxHeight / actualHeight;                actualWidth = (int) (imgRatio * actualWidth);               actualHeight = (int) maxHeight;             } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Visitor Management/VisitorPicture");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + "IMG_"+System.currentTimeMillis() + ".png");
        return uriSting;

    }
    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.i(TAG, "Selected File Path:" + selectedFilePath);

        String name = data.getData().getPath();
        Log.d("IMAGEPIC", name);

        imageButton.setImageBitmap(bm);
    }







    private String applicationFormMY(String checkImageURL,String name,String email,String phone) throws IOException {

        String s = null;

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        String Tag="CHANTI";
        URL url = new URL(AppUrls.TEMP_REGISTRATIOM);
        Log.d("CHECKVALUE", checkImageURL+","+name+","+email+","+phone);


        FileInputStream fileInputStream = new FileInputStream(checkImageURL);
        try
        {
            Log.e(Tag,"Starting Http File Sending to URL");

            // Open a HTTP connection to the URL
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();

            // Allow Inputs
            conn.setDoInput(true);

            // Allow Outputs
            conn.setDoOutput(true);

            // Don't use a cached copy.
            conn.setUseCaches(false);

            // Use a post method.
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Connection", "Keep-Alive");

            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"name\""+ lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(name);
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"email\""+ lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(email);
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"phone\""+ lineEnd);
            dos.writeBytes(lineEnd);
            dos.writeBytes(phone);
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);


            String filename = checkImageURL.substring(checkImageURL.lastIndexOf("/") + 1);
            dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\"" + filename +"\"" + lineEnd);
            dos.writeBytes(lineEnd);

            Log.e(Tag,"Headers are written");

            // create a buffer of maximum size
            int bytesAvailable = fileInputStream.available();

            int maxBufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[ ] buffer = new byte[bufferSize];

            // read file and write it into form...
            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0,bufferSize);
            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams
            fileInputStream.close();

            dos.flush();

            Log.e(Tag,"File Sent, Response: "+String.valueOf(conn.getResponseCode()));
            int responceCode = conn.getResponseCode();
            Log.d("HAPPYBIRTHDAY", responceCode+"");
            if(responceCode == 200) {
                InputStream is = conn.getInputStream();

                // retrieve the response from server
                int ch;

                StringBuffer b = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    b.append((char) ch);
                }
                s = b.toString();
                Log.d("-------Response", s);
                dos.close();
            }
            if(responceCode == 400)
            {
                InputStream errorstream = conn.getErrorStream();
                int ch;

                StringBuffer b = new StringBuffer();
                while ((ch = errorstream.read()) != -1) {
                    b.append((char) ch);
                }
                s = b.toString();
                Log.d("*******Response", s);
                dos.close();
            }
        }
        catch (MalformedURLException ex)
        {
            Log.e(Tag, "URL error: " + ex.getMessage(), ex);
        }

        catch (IOException ioe)
        {
            Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
        }
        return s;

    }
}
