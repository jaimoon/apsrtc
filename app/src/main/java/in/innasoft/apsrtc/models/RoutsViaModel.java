package in.innasoft.apsrtc.models;

/**
 * Created by purushotham on 6/12/16.
 */

public class RoutsViaModel
{
    public String id;
    public String via;
    public String kilometers;
    public String price;
    public String id_charges;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getKilometers() {
        return kilometers;
    }

    public void setKilometers(String kilometers) {
        this.kilometers = kilometers;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId_charges() {
        return id_charges;
    }

    public void setId_charges(String id_charges) {
        this.id_charges = id_charges;
    }
}
