package in.innasoft.apsrtc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.innasoft.apsrtc.MainActivity;
import in.innasoft.apsrtc.R;
import in.innasoft.apsrtc.activities.AboveSSC;
import in.innasoft.apsrtc.activities.BelowSSC;
import in.innasoft.apsrtc.activities.DemoRegistration;
import in.innasoft.apsrtc.activities.InstructionActivity;
import in.innasoft.apsrtc.activities.MyPassActivity;
import in.innasoft.apsrtc.activities.QRScannerActivity;
import in.innasoft.apsrtc.activities.SearchForm;
import in.innasoft.apsrtc.models.HomeModel;

public class HomeAdapter  extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {
    private Context mContext;
    private List<HomeModel> homeList;
    // ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbNail;
        TextView txtTitle,txtDescription;
        CardView tagLayout;


        public MyViewHolder(View view) {
            super(view);

            thumbNail = (ImageView) view.findViewById(R.id.thumbnail);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            tagLayout = (CardView) view.findViewById(R.id.tagLayout);
            txtDescription = (TextView) view.findViewById(R.id.txtDescription);

        }
    }

    public HomeAdapter(Context mContext, List<HomeModel> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeModel home = homeList.get(position);

        holder.txtTitle.setText(home.getTitle());
        holder.txtDescription.setText(home.getDescription());
        holder.thumbNail.setImageResource(home.getImage());
        holder.tagLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position==0){
                    Intent intent = new Intent(mContext, SearchForm.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else if (position==1){
                    Intent intent = new Intent(mContext, AboveSSC.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else if (position==2){
                    Intent intent = new Intent(mContext, BelowSSC.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else if (position==3){
                    Intent intent = new Intent(mContext, QRScannerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);

                   /* Intent i = null;
                    boolean isTablet = mContext.getResources().getBoolean(R.bool.is_tablet);
                    if (!isTablet)
                        i = new Intent(this, MainActivityPhone.class);
                    else
                        i = new Intent(this, MainActivity.class);
                    startActivity(i);*/
                }
                else if (position==4){

                }else if (position==5){
                    Intent intent = new Intent(mContext, MyPassActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else if (position==6){
                    Intent intent = new Intent(mContext, DemoRegistration.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
                else if (position==7){
                    Intent intent = new Intent(mContext, InstructionActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }

            }
        });



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }



}
